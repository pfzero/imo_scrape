// babel polyfill
require('babel-polyfill');

// load custom polyfills
require('./polyfills');

// require and start the app
require('./main');

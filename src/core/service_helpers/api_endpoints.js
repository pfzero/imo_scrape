import _ from "lodash";
import config from "config";
import url from "url";
import querystring from "querystring";

const ENDPOINTS_CONFIGS = config.services.imo_api.endpoints;
const TOKEN = config.services.imo_api.token;

/**
 * getApiEndpointTpl returns the endpoint template for a given api call
 * @example
 * 	let tpl = getApiEndpointTpl('spider.ping')
 * 	let endpoint = tpl();
 * 	console.log(endpoint); // -> 127.0.0.1/api/spider/ping
 * 	// or
 * 	tpl = getApiEndpoint('crawledEstate.update')
 * 	let endpoint = tpl({id: 5});
 * 	console.log(endpoint); // -> 127.0.0.1/api/crawled-estates/id/5
 * 	
 * @param  {string} resourceAction path to endpoint from configs
 * @return {Function}              template fn that can be called with 
 *                                 required params to obtain the endpoint
 */
export default function getApiEndpointTpl(resourceAction) {
  if (!_.isString(resourceAction)) {
    throw new Error(
      "unsupported type for `resourceAction` passed: " + typeof resourceAction
    );
  }
  let parts = resourceAction.split(".");
  let baseParts = [parts[0], "base"];
  let endpoint =
    ENDPOINTS_CONFIGS.base +
    _.get(ENDPOINTS_CONFIGS, baseParts) +
    _.get(ENDPOINTS_CONFIGS, parts);
  let parsedURL = url.parse(endpoint);
  let query = querystring.parse(parsedURL.query);
  query.token = TOKEN;
  let search = "";
  Object.keys(query).forEach(key => {
    let append = `${key}=${query[key]}`;
    if (search === "") {
      search += append;
    } else {
      search += `&${append}`;
    }
  });
  parsedURL.search = search;
  let endpointTpl = url.format(parsedURL);
  return _.template(decodeURI(endpointTpl));
}

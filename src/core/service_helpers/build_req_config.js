import config from "config";

export default function buildReqConfig(url) {
  return {
    url,
    headers: { "User-Agent": config.crawler_general_settings.userAgent },
    json: true
  };
}

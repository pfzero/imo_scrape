import config from "config";
import { RateLimiter } from "utils";

const LIMITER = new RateLimiter(config.services.imo_api.rateLimit || 0);

export default LIMITER;

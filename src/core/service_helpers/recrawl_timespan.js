import moment from "moment";
import config from "config";

// set the interval when a property needs to be recrawled
let timeSpanValues = config.crawler_schedulers.reCrawlTimespan.split(" ");

const getTimeSpan = () => {
  return moment().subtract(timeSpanValues[0], timeSpanValues[1]).toDate();
};
export default getTimeSpan;

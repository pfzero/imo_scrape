import logger from "winston";
import _ from "lodash";
import { getCollection } from "core/services/db/db";
import Property from "core/resources/estates/property";
import getTimeSpan from "core/service_helpers/recrawl_timespan";

// the db collection on which we'll operate
const PropertiesCol = getCollection("properties");

/**
 * getPropertyByURL queries the db and fetches the property
 * matching the given url;
 * @param  {String} url 
 * @return {Property}     
 */
async function getPropertyByURL(url) {
  return new Promise((resolve, reject) => {
    PropertiesCol.findOne({ url }, function(err, doc) {
      if (err) {
        reject(err);
      }
      Property.fromDB(doc).then(resolve).catch(reject);
    });
  });
}

/**
 * insert a Property in database
 *
 * @private 	use save method instead
 * @param  {Object} parsedProperty
 * @return {Promise<void>}
 */
async function insert(parsedProperty) {
  return new Promise((resolve, reject) => {
    PropertiesCol.insert(parsedProperty, (err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(true);
    });
  });
}

/**
 * update() a property by id; The update is full, i.e.
 * the database object is replaced by the given one
 *
 * @private  use save method instead
 * @param  {String} id
 * @param  {Object} parsedProperty
 * @return {Promise<void>}
 */
async function update(id, parsedProperty) {
  return new Promise((resolve, reject) => {
    PropertiesCol.update({ _id: id }, parsedProperty, (err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(true);
    });
  });
}

/**
 * patch() partially updates a property by the given id
 *
 * @private use save method instead
 * @param  {String} id
 * @param  {Object} modifications
 * @return {Promise<void>}
 */
async function patch(id, modifications) {
  return new Promise((resolve, reject) => {
    PropertiesCol.update({ _id: id }, { $set: modifications }, (err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(true);
    });
  });
}

/**
 * save() saves the given property; It handles all the logic
 * for determining wether the given property needs to just be
 * inserted in database or it must be updated
 * 
 * @param  {Object} property the property coming from parser
 * @return {Promise<void>}
 */
async function save(property) {
  let dbProperty;
  try {
    dbProperty = await getPropertyByURL(property.url);

    // if the property isn't relevant and it wasn't found
    // in db
    if (!property.isRelevant && dbProperty.isEmpty()) {
      return false;
    }

    // if the property is not relevant anymore but we have
    // the same property already stored in database;
    if (!property.isRelevant && !dbProperty.isEmpty()) {
      property.setUpdatedAt().setLastParsed().setEtag();
      let jsonedProp = property.toJSON();
      return await patch(dbProperty._id, _.omit(jsonedProp, ["extracted"]));
    }

    // if the property is relevant and is not in db;
    if (property.isRelevant && dbProperty.isEmpty()) {
      property.setUpdatedAt().setLastParsed().setEtag();
      let jsonedProp = property.toJSON();
      return await insert(jsonedProp);
    }

    // if the property is relevant and it was already saved
    // in db, just update the property in db;
    property.setUpdatedAt().setLastParsed().setEtag();

    // if it hasn't changed, just update the lastParsed date;
    if (!property.hasChanged(FdbProperty.etag)) {
      let toPatch = { lastParsed: property.lastParsed };
      return await patch(dbProperty._id, toPatch);
    }

    // if changed, perform a full update;
    property._id = dbProperty._id;
    property.createdAt = dbProperty.createdAt;
    return await update(property._id, property);
  } catch (e) {
    logger.error(`failed to save property ${crawledProperty.url}`);
    if (!!dbProperty && !dbProperty.isEmpty()) {
      logger.info(`property found in db with id ${dbProperty._id}`);
    }
    if (!!property && !property.isEmpty()) {
      logger.info(`property crawled: ${JSON.stringify(property)}`);
    }
    logger.error(e.stack);
    throw e;
  }
}

/**
 * getPropertiesToReCrawl() returns the properties that
 * need to be recrawled based on the given timespan set in
 * configurations
 * @return {Promise<Properties>}
 */
async function getPropertiesToReCrawl(spiderKey) {
  let query = { isRelevant: true, lastParsed: { $lt: getTimeSpan() } };
  if (!!spiderKey) {
    query.spider = spiderKey;
  }
  return new Promise((resolve, reject) => {
    PropertiesCol.find(query, { url: 1, spider: 1, _id: 0 }, (err, urls) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(urls);
    });
  });
}

export default {
  save,
  getPropertyByURL,
  getPropertiesToReCrawl
};

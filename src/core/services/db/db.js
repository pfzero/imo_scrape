import logger from "winston";
import mongo from "mongojs";
import config from "config";

// getConnStr builds the mongo database connection
// string from config
const getConnStr = () => {
  const { host, port, user, pass, database } = config.db.mongo;
  if (user) {
    return `${user}:${pass}@${host}:${port}/${database}`;
  } else {
    return `${host}:${port}/${database}`;
  }
};

// db singleton object
let db;

/**
 * getDB connects to the mongo db database
 * and returns the database object;
 * @return {Mongo.DB} the db object
 */
export function getDB() {
  if (!!db) {
    return db;
  }
  const connStr = getConnStr();
  db = mongo(connStr);
  return db;
}

/**
 * getCollection gets the collection from database
 * object;
 * @param  {String} 			  col collection name
 * @return {Mongo.Collection}     the mongoDB collection
 */
export function getCollection(col) {
  const db = getDB();
  return db.collection(col);
}

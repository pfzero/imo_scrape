import logger from "winston";
import _ from "lodash";
import { getCollection } from "core/services/db/db";
import EstateAd from "core/resources/estate_ads/estate_ad";
import getTimeSpan from "core/service_helpers/recrawl_timespan";

// the db collection on which we'll operate
const AdsCol = getCollection("estate_ads");

/**
 * getPropertyByURL queries the db and fetches the property
 * matching the given url;
 * @param  {string} url 
 * @return {Promise<Property>}     
 */
async function getEstateAdByURL(url) {
  return new Promise((resolve, reject) => {
    AdsCol.findOne({ url }, function(err, doc) {
      if (err) {
        return reject(err);
      }
      EstateAd.fromDB(doc).then(resolve).catch(reject);
    });
  });
}

/**
 * getPropertyByEtag queries the db and fetches the ad by etag;
 * @param {string} etag 
 * @return {Promise<EstateAd>}
 */
async function getEstateAdByEtag(etag) {
  return new Promise((resolve, reject) => {
    AdsCol.findOne({ etag }, (err, doc) => {
      if (err) {
        return reject(err);
      }
      return EstateAd.fromDB(doc).then(resolve).catch(reject);
    });
  });
}

/**
 * insert a EstateAd in database
 *
 * @private 	use save method instead
 * @param  {Object} parsedAd
 * @return {Promise<void>}
 */
async function insert(parsedAd) {
  return new Promise((resolve, reject) => {
    AdsCol.insert(parsedAd, (err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(true);
    });
  });
}

/**
 * update() an estate ad by id; The update is full, i.e.
 * the database object is replaced by the given one
 *
 * @private  use save method instead
 * @param  {String} id
 * @param  {Object} estateAd
 * @return {Promise<void>}
 */
async function update(id, estateAd) {
  return new Promise((resolve, reject) => {
    AdsCol.update({ _id: id }, estateAd, (err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(true);
    });
  });
}

/**
 * patch() partially updates an estate ad by the given id
 *
 * @private use save method instead
 * @param  {String} id
 * @param  {Object} modifications
 * @return {Promise<void>}
 */
async function patch(id, modifications) {
  return new Promise((resolve, reject) => {
    AdsCol.update({ _id: id }, { $set: modifications }, (err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(true);
    });
  });
}

/**
 * save() saves the given estate ad; It handles all the logic
 * for determining wether the given estate ad needs to just be
 * inserted in database or it must be updated
 * 
 * @param  {EstateAd} estateAd the estate ad coming from parser
 * @return {Promise<void>}
 */
async function save(estateAd) {
  let dbEstateAd;
  try {
    dbEstateAd = await getEstateAdByURL(estateAd.url);
    dbEstateAd = dbEstateAd || (await getEstateAdByEtag(estateAd.etag));

    // if the estate ad isn't relevant and it wasn't found
    // in db
    if (!estateAd.isRelevant && dbEstateAd.isEmpty()) {
      return false;
    }

    // if the estate ad is not relevant anymore but we have
    // the same estate ad already stored in database;
    if (!estateAd.isRelevant && !dbEstateAd.isEmpty()) {
      estateAd.setUpdatedAt().setLastParsed().setEtag();
      let jsonedAd = estateAd.toJSON();
      return await patch(dbEstateAd._id, _.omit(jsonedAd, ["extracted"]));
    }

    // if the estate ad is relevant and is not in db;
    if (estateAd.isRelevant && dbEstateAd.isEmpty()) {
      estateAd.setUpdatedAt().setLastParsed().setEtag();
      let jsonedAd = estateAd.toJSON();
      return await insert(jsonedAd);
    }

    // if the estate ad is relevant and it was already saved
    // in db, just update the estate ad in db;
    estateAd.setUpdatedAt().setLastParsed().setEtag();

    // if it hasn't changed, just update the lastParsed date;
    if (!estateAd.hasChanged(dbEstateAd.etag)) {
      let toPatch = { lastParsed: estateAd.lastParsed };
      return await patch(dbEstateAd._id, toPatch);
    }

    // if changed, perform a full update;
    estateAd._id = dbEstateAd._id;
    estateAd.createdAt = dbEstateAd.createdAt;
    return await update(estateAd._id, estateAd);
  } catch (e) {
    logger.error(`failed to save estateAd ${estateAd.url}`);
    if (!!dbEstateAd && !dbEstateAd.isEmpty()) {
      logger.info(`estate ad found in db with id ${dbEstateAd._id}`);
    }
    if (!!estateAd && !estateAd.isEmpty()) {
      logger.info(`estate ad crawled: ${JSON.stringify(estateAd)}`);
    }
    logger.error(e.stack);
    throw e;
  }
}

/**
 * getEstateAdsToRecrawl() returns the estate ads that
 * need to be recrawled based on the given timespan set in
 * configurations
 * @return {Promise<Array<url>>}
 */
async function getEstateAdsToRecrawl(spiderKey) {
  let query = { isRelevant: true, lastParsed: { $lt: getTimeSpan() } };
  if (!!spiderKey) {
    query.spider = spiderKey;
  }
  return new Promise((resolve, reject) => {
    AdsCol.find(query, { url: 1, spider: 1, _id: 0 }, (err, urls) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(urls);
    });
  });
}

export default {
  save,
  getEstateAdByURL,
  getEstateAdsToRecrawl
};

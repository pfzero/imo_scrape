import Req from "request-promise";
import config from "config";
import _ from "lodash";
import { RateLimiter } from "utils";

const limiter = new RateLimiter(config.services.nominatim.rateLimit || 0);
const reverseTpl = _.template(config.services.nominatim.reverseGeo);

const getReverseGeoEndpoint = coords => {
  return reverseTpl(coords);
};
const buildReqConfig = uri => {
  return {
    uri,
    headers: { "User-Agent": config.crawler_general_settings.userAgent },
    json: true
  };
};

const formatAddress = response => {
  let addr = response.address;
  addr.display_name = response.display_name;
  return addr;
};

function reverse({ lat, lng }) {
  let config = buildReqConfig(getReverseGeoEndpoint({ lat, lng }));
  return limiter.add(() => {
    return Req.get(config).then(formatAddress);
  });
}

export default {
  reverse
};

import { objToCamelCase } from "utils";
import _ from "lodash";

/**
 * fromBackend transforms the given object received from backend
 * to a crawled property oject type; 
 * @param  {Object} obj object received from backend
 * @return {Object}     object compatible with crawlers
 */
export function fromBackendEstate(obj) {
  let camelCased = objToCamelCase(obj);
  let transformed = _.pick(camelCased, [
    "id",
    "isValid",
    "isRelevant",
    "etag",
    "state",
    "lastParsed",
    "updatedAt",
    "isNotFound",
    "isSuccessfullParse",
    "url"
  ]);
  transformed.spider = camelCased.spider.name;
  transformed.extracted = _.pick(camelCased, [
    "title",
    "description",
    "priceIsPrivate",
    "priceIncludesVat",
    "price",
    "meta"
  ]);
  let extracted = transformed.extracted;
  extracted.category = {
    key: camelCased.type.shortName,
    label: camelCased.type.name
  };
  extracted.sellerAgentName = camelCased.contact.name;
  extracted.sellerAgentPhone = camelCased.contact.phone;
  extracted.sellerAgentEmail = camelCased.contact.email;
  extracted.priceCurrency = camelCased.priceCurrency.Name;
  extracted.mapCoordinates = {
    lat: camelCased.latitude,
    lng: camelCased.longitude
  };
  extracted.address = {
    town: camelCased.city,
    city: camelCased.city,
    county: camelCased.region,
    displayName: camelCased.address,
    postcode: camelCased.zip,
    country: camelCased.country
  };
  if (camelCased.images instanceof Array) {
    extracted.images = camelCased.images
      .filter(image => !image.isMainImage && image.url !== "")
      .map(image => image.url);
    extracted.mainImg = camelCased.images.find(
      image => image.isMainImage && image.url !== ""
    );
    extracted.mainImg = extracted.mainImg && extracted.mainImg.url;
  } else {
    extracted.images = [];
    extracted.mainImg = "";
  }
  return transformed;
}

/**
 * toBackend transforms a crawled property type object to one that
 * is compatible with the backend;
 * @param  {Object} obj the crawled object
 * @return {Object}     the object that is compat with backend
 */
export function toBackendEstate(obj) {
  let camelCased = objToCamelCase(obj);
  let transformed = _.omit(camelCased, [
    "id",
    "_id",
    "extracted",
    "spider",
    "state",
    "createdAt",
    "updatedAt"
  ]);
  transformed.spider = { name: camelCased.spider };

  // check for extracted object
  if (_.isEmpty(camelCased.extracted)) {
    return transformed;
  }
  let extracted = {};
  let mainProps = _.pick(camelCased.extracted, [
    "title",
    "description",
    "price",
    "originalProductCode",
    "priceIsPrivate",
    "priceIncludesVat",
    "meta"
  ]);
  // copy values
  Object.keys(mainProps).forEach(key => {
    extracted[_.camelCase(key)] = mainProps[key];
  });
  extracted.type = { shortName: camelCased.extracted.category.key };
  extracted.priceCurrency = { name: camelCased.extracted.priceCurrency };
  extracted.contact = {
    name: camelCased.extracted.sellerAgentName,
    email: camelCased.extracted.sellerAgentEmail,
    phone: camelCased.extracted.sellerAgentPhone
  };
  extracted.images = camelCased.extracted.images.map(image => {
    return { isMainImage: false, url: image };
  });
  if (camelCased.extracted.mainImg) {
    extracted.images.push({
      isMainImage: true,
      url: camelCased.extracted.mainImg
    });
  }
  extracted.address = camelCased.extracted.address.displayName;
  extracted.city =
    camelCased.extracted.address.city ||
    camelCased.extracted.address.town ||
    camelCased.extracted.address.village;
  extracted.region = camelCased.extracted.address.county;
  extracted.zip = camelCased.extracted.address.postcode;
  extracted.country = camelCased.extracted.address.country;
  extracted.latitude = camelCased.extracted.mapCoordinates.lat;
  extracted.longitude = camelCased.extracted.mapCoordinates.lng;
  return _.merge(transformed, extracted);
}

import Property from "core/resources/estates/property";
import { getApiEndpointTpl } from "core/service_helpers";
import { fromBackendEstate, toBackendEstate } from "./estate_backend_mapping";
import CrawledEntityAPI from "./crawled_entity";

/**
 * CrawledEstateAPI is the api needed to manipulate the crawled estate entity type;
 */
export default class CrawledEstateAPI extends CrawledEntityAPI {
  /**
     * @param {Object} entity 
     * @return {Promise<Object>}
     * @protected
     */
  async _entityToBackend(entity) {
    return toBackendEstate(entity);
  }

  /**
     * @param {Object} backendEntity 
     * @return {Promise<Property>}
     * @protected
     */
  async _entityFromBackend(backendEntity) {
    let transformed = fromBackendEstate(backendEntity);
    return await Property.fromBackend(transformed);
  }

  /**
     * _entityFromJSON transforms the given property obtained
     * from crawling and transforms it to its respective model
     * instance;
     * @param {Object} entity the crawled entity
     * @protected
     */
  async _entityFromJSON(entity) {
    return await Property.fromJSON(entity);
  }

  constructor() {
    super();
    this.getByFieldTpl = getApiEndpointTpl("crawledEstate.getByField");
    this.createTpl = getApiEndpointTpl("crawledEstate.create");
    this.updateTpl = getApiEndpointTpl("crawledEstate.update");
    this.getCrawledEntitiesTpl = getApiEndpointTpl(
      "crawledEstate.getCrawledProperties"
    );
    this.getEntitiesToReCrawlTpl = getApiEndpointTpl(
      "crawledEstate.getPropertiesToRecrawl"
    );
  }
}

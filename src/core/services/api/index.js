import CrawledEstateAdAPI from "./crawled_estate_ad";
import CrawledEstateAPI from "./crawled_estate";
import SpiderAPI from "./spider";
import { heartbeat } from "./general";

export default {
  heartbeat,
  spider: new SpiderAPI(),
  crawledEstate: new CrawledEstateAPI(),
  crawledEstateAd: new CrawledEstateAdAPI()
};

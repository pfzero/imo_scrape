import config from "config";
import Req from "request-promise";

export async function heartbeat() {
  let endpoint = config.services.imo_api.endpoints.heartbeat;
  return Req.get(endpoint);
}

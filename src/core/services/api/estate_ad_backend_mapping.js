import { objToCamelCase } from "utils";
import _ from "lodash";

/**
 * fromBackend transforms the given object received from backend
 * to a crawled estate ad oject type; 
 * @param  {Object} obj object received from backend
 * @return {Object}     object compatible with crawlers
 */
export function fromBackendEstateAd(obj) {
  let camelCased = objToCamelCase(obj);
  let transformed = _.pick(camelCased, [
    "id",
    "isValid",
    "isRelevant",
    "etag",
    "state",
    "lastParsed",
    "updatedAt",
    "isNotFound",
    "isSuccessfullParse",
    "url"
  ]);
  transformed.spider = camelCased.spider.name;
  transformed.extracted = _.pick(camelCased, [
    "title",
    "description",
    "category",
    "price",
    "priceIncludesVat",
    "city",
    "contactName",
    "contactPhone",
    "contactEmail"
  ]);
  transformed.extracted.priceCurrency = camelCased.priceCurrency.Name;
  return transformed;
}

/**
 * toBackend transforms a crawled property type object to one that
 * is compatible with the backend;
 * @param  {Object} obj the crawled object
 * @return {Object}     the object that is compat with backend
 */
export function toBackendEstateAd(obj) {
  let camelCased = objToCamelCase(obj);
  let transformed = _.omit(camelCased, [
    "id",
    "_id",
    "extracted",
    "spider",
    "state",
    "createdAt",
    "updatedAt"
  ]);
  transformed.spider = { name: camelCased.spider };

  // check for extracted object
  if (_.isEmpty(camelCased.extracted)) {
    return transformed;
  }
  let extracted = {};
  let mainProps = _.pick(camelCased.extracted, [
    "title",
    "description",
    "category",
    "price",
    "priceIncludesVat",
    "city",
    "contactName",
    "contactPhone",
    "contactEmail"
  ]);
  // copy values
  Object.keys(mainProps).forEach(key => {
    extracted[key] = mainProps[key];
  });

  extracted.priceCurrency = { name: camelCased.extracted.priceCurrency };

  return _.merge(transformed, extracted);
}

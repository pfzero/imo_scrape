import EstateAd from "core/resources/estate_ads/estate_ad";
import { getApiEndpointTpl } from "core/service_helpers";
import {
  fromBackendEstateAd,
  toBackendEstateAd
} from "./estate_ad_backend_mapping";

import CrawledEntityAPI from "./crawled_entity";

/**
 * CrawledEstateAPI is the api needed to manipulate the crawled estate entity type;
 */
export default class CrawledEstateAdAPI extends CrawledEntityAPI {
  /**
     * @param {Object} entity 
     * @return {Promise<Object>}
     * @protected
     */
  async _entityToBackend(entity) {
    return toBackendEstateAd(entity);
  }

  /**
     * @param {Object} backendEntity 
     * @return {Promise<EstateAd>}
     * @protected
     */
  async _entityFromBackend(backendEntity) {
    let transformed = fromBackendEstateAd(backendEntity);
    return await EstateAd.fromBackend(transformed);
  }

  /**
     * _entityFromJSON transforms the given estate ad obtained
     * from crawling and transforms it to its respective model
     * instance;
     * @param {Object} entity the crawled entity
     * @protected
     */
  async _entityFromJSON(entity) {
    return await EstateAd.fromJSON(entity);
  }

  constructor() {
    super();
    this.getByFieldTpl = getApiEndpointTpl("crawledEstateAd.getByField");
    this.createTpl = getApiEndpointTpl("crawledEstateAd.create");
    this.updateTpl = getApiEndpointTpl("crawledEstateAd.update");
    this.getCrawledEntitiesTpl = getApiEndpointTpl(
      "crawledEstateAd.getCrawledEstateAds"
    );
    this.getEntitiesToReCrawlTpl = getApiEndpointTpl(
      "crawledEstateAd.getEstateAdsToRecrawl"
    );
  }
}

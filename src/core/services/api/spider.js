import Req from "request-promise";
import {
  getApiEndpointTpl,
  buildReqConfig,
  LIMITER
} from "core/service_helpers";

export default class SpiderAPI {
  constructor() {
    this.pingTpl = getApiEndpointTpl("spider.ping");
    this.registerSpiderTpl = getApiEndpointTpl("spider.create");
  }

  async ping(spiderName) {
    let endpointTpl = getApiEndpointTpl("spider.ping");
    let endpoint = endpointTpl({ spiderName });
    return LIMITER.add(() => Req.get(buildReqConfig(endpoint)));
  }

  async registerSpider(spiderName, spiderDescription, spiderType, host) {
    let endpointTpl = getApiEndpointTpl("spider.create");
    let endpoint = endpointTpl({
      spiderName,
      spiderDescription,
      spiderType,
      host
    });
    return LIMITER.add(() => Req.get(buildReqConfig(endpoint)));
  }
}

import _ from "lodash";
import config from "config";
import Req from "request-promise";
import logger from "winston";

import { LIMITER, buildReqConfig } from "core/service_helpers";
import { objToCamelCase } from "utils";

/**
 * EntityAPI is the base class used for manipulating a crawled entity
 * between the crawler and the backend api;
 * 
 * @abstract
 */
export default class CrawledEntityAPI {
  // tpl functions used for determining the endpoints for:
  // get entity by field name and value
  getByFieldTpl = () => {};
  // create entity
  createTpl = () => {};
  // update entity
  updateTpl = () => {};
  // get entities that needs to be recrawled
  getEntitiesToReCrawlTpl = () => {};
  // get entities crawled
  getCrawledEntitiesTpl = () => {};

  /**
     * _entityToBackend transforms the given entity object to
     * backend compatible object
     * @param {Object} entity 
     * @protected
     */
  async _entityToBackend(entity) {}

  /**
     * _entityFromBackend transforms the given entity obtained
     * from the backend API to its respective model instance;
     * @param {Object} entity 
     * @protected
     */
  async _entityFromBackend(entity) {}

  /**
     * _entityFromJSON transforms the given entity obtained
     * from crawling and transforms it to its respective model
     * instance;
     * @param {Object} entity the crawled entity
     * @protected
     */
  async _entityFromJSON(entity) {}

  /**
     * getEntityBy returns the entity from backend searched by the given k,v
     * @param {string} field 
     * @param {any} value 
     */
  async getEntityBy(field, value) {
    let endpointTpl = this.getByFieldTpl;
    let endpoint = endpointTpl({
      field: field,
      value: encodeURIComponent(value)
    });
    return LIMITER.add(() => {
      return Req.get(buildReqConfig(endpoint)).then(entity => {
        return this._entityFromBackend(entity);
      });
    });
  }

  /**
     * getEntityByURL finds entity in the backend
     * that has the same url
     * @param {string} url
     * @return {Promise<Entity>}
     */
  async getEntityByURL(url) {
    return this.getEntityBy("url", url);
  }

  /**
     * getEntityByEtag finds the entity in backend having the
     * same etag
     * @param {string} etag 
     */
  async getEntityByEtag(etag) {
    return this.getEntityBy("etag", etag);
  }

  /**
     * insert adds the given parsedEntity to the backend
     * @param {Entity} parsedEntity 
     * @return {Promise<HttpResponse>}
     */
  async insert(parsedEntity) {
    let endpointTpl = this.createTpl;
    let endpoint = endpointTpl();
    let data = buildReqConfig(endpoint);
    data.form = JSON.stringify(parsedEntity);
    data.method = "post";
    return LIMITER.add(() => Req(data));
  }

  /**
     * update updates the given entity with the given id 
     * @param {number} id 
     * @param {Object} parsedEntity
     * @return {Promise<boolean>}
     */
  async update(id, parsedEntity) {
    let endpointTpl = this.updateTpl;
    let endpoint = endpointTpl({ id });
    let data = buildReqConfig(endpoint);
    data.form = JSON.stringify(parsedEntity);
    data.method = "put";
    return LIMITER.add(() => Req(data).then(() => true));
  }

  /**
     * patch is alias to update
     * @param {number} id 
     * @param {Object} toPatch 
     * @return {Promise<boolean>}
     */
  async patch(id, toPatch) {
    return this.update(id, toPatch);
  }

  /**
     * save() saves the given entity; It handles all the logic
     * for determining wether the given entity needs to just be
     * inserted in backend api or it must be updated
     * 
     * @param  {Entity} crawledEntity the entity coming from parser
     * @return {Promise<boolean>}
     */
  async save(crawledEntity) {
    let apiEntity;

    // get entity from backend;
    try {
      crawledEntity.setEtag();
      apiEntity = await this.getEntityByURL(crawledEntity.url);
    } catch (e) {
      if (e.statusCode !== 404) {
        throw e;
      }
    }

    // try to find the entity by etag
    try {
      apiEntity = apiEntity || (await this.getEntityByEtag(crawledEntity.etag));
    } catch (e) {
      if (e.statusCode !== 404) {
        throw e;
      }
      apiEntity = apiEntity || (await this._entityFromJSON({}));
    }

    try {
      // if the crawledEntity isn't relevant and it wasn't found
      // in backend
      if (!crawledEntity.isRelevant && apiEntity.isEmpty()) {
        return false;
      }

      // if the crawledEntity is not relevant anymore but we have
      // the same crawledEntity already stored in database;
      if (!crawledEntity.isRelevant && !apiEntity.isEmpty()) {
        crawledEntity.setLastParsed();
        let jsonedEntity = crawledEntity.toJSON();
        delete jsonedEntity.updatedAt;
        delete jsonedEntity.createdAt;
        // ToDO: check if we need to update the entity
        // to be deleted state;
        jsonedEntity = await this._entityToBackend(
          _.omit(jsonedEntity, ["extracted"])
        );
        return await this.patch(apiEntity.id, jsonedEntity);
      }

      // if the crawledEntity is relevant and is not in backend;
      if (crawledEntity.isRelevant && apiEntity.isEmpty()) {
        crawledEntity.setLastParsed();
        let jsonedEntity = crawledEntity.toJSON();
        delete jsonedEntity.updatedAt;
        delete jsonedEntity.createdAt;
        jsonedEntity = await this._entityToBackend(jsonedEntity);
        await this.insert(jsonedEntity);
        return true;
      }

      // if the crawledEntity is relevant and it was already saved
      // in backend, just update the entity in backend;
      crawledEntity.setLastParsed();

      // if it hasn't changed, just update the lastParsed date;
      if (!crawledEntity.hasChanged(apiEntity.etag)) {
        let toPatch = { lastParsed: crawledEntity.lastParsed };
        return await this.patch(apiEntity.id, toPatch);
      }

      // if changed, perform a full update;
      crawledEntity.id = apiEntity.id;
      delete crawledEntity.updatedAt;
      delete crawledEntity.createdAt;
      let toUpdate = crawledEntity.toJSON();
      return await this.update(
        crawledEntity.id,
        await this._entityToBackend(toUpdate)
      );
    } catch (e) {
      logger.error(`failed to save crawledEntity ${crawledEntity.url}`);
      if (!!apiEntity && !apiEntity.isEmpty()) {
        logger.info(`entity found in backend with id ${apiEntity.id}`);
      }
      if (!!crawledEntity && !crawledEntity.isEmpty()) {
        logger.info(`crawledEntity crawled: ${JSON.stringify(crawledEntity)}`);
      }
      logger.error(e.stack);
      throw e;
    }
  }

  /**
     * getEntitiesToReCrawl fetches all the entities that needs
     * to be re-crawled;
     * @param {string} spiderName 
     */
  async getEntitiesToReCrawl(spiderName) {
    let endpointTpl = this.getEntitiesToReCrawlTpl;
    let endpoint = endpointTpl({ spiderName });
    return LIMITER.add(() =>
      Req.get(buildReqConfig(endpoint)).then(urls => {
        return urls.map(objToCamelCase).map(url => {
          url.spider = url.spiderName;
          delete url.spiderName;
          return url;
        });
      })
    );
  }

  /**
     * getCrawledEntities fetches all the entities that
     * were already crawled and saved to backend api for the
     * given spider;
     * @param {string} spiderName 
     */
  async getCrawledEntities(spiderName) {
    let endpointTpl = this.getCrawledEntitiesTpl;
    let endpoint = endpointTpl({ spiderName });
    return LIMITER.add(() =>
      Req.get(buildReqConfig(endpoint)).then(urls => {
        return urls.map(objToCamelCase).map(url => {
          url.spider = url.spiderName;
          delete url.spiderName;
          return url;
        });
      })
    );
  }
}

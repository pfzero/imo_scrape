import logger from "winston";
import config from "config";
import Rx from "rxjs/Rx";
import _ from "lodash";

import CrawlerManager from "core/crawlers/crawlers_manager";
import ParserManager from "core/parsers/parsers_manager";
import API from "core/services/api";
import Property from "core/resources/estates/property";
import EstateAd from "core/resources/estate_ads/estate_ad";
import PropertyStore from "core/services/db/properties";
import EstateAdStore from "core/services/db/ads";

import validateExtracted from "core/validators";
import { strToInterval, objToCamelCase } from "utils";

// create heartbeat request every 10 seconds
const CHECK_API_INTERVAL = 1000 * 10;

// interval at which spiders will inform the API Service of their
// availability;
const SPIDERS_HB_INTERVAL = strToInterval(config.crawler_schedulers.pingSpider);

// interval at which all crawlers are restarted;
const RESTART_INTERVAL = strToInterval(config.crawler_schedulers.restart);

// interval at which old properties are re-crawled;
const RECRAWL_INTERVAL = strToInterval(
  config.crawler_schedulers.reCrawlProperties
);

/**
 * Daemon is the daemon process that glues all the pieces within the app.
 * It starts the crawlerManager, listens for crawled pages and passes them
 * to the parserManager which'll transform the raw html into structured 
 * object. Then, it will send the result to the service which will save the
 * pages in database;
 */
export default class Daemon {
  /**
	 * __configureApiService configures and checks wether the API service is running
	 * 
	 * @private
	 */
  async __configureApiService() {
    if (this.isAPIServiceOn) {
      return;
    }
    try {
      await API.heartbeat();
      this.isAPIServiceOn = true;
      await this.__registerSpiders();
    } catch (e) {
      this.isAPIServiceOn = false;
    }
  }

  /**
	 * __registerSpiders establishes a hearbeat connection from each spider
	 * to the API service, so that the spiders can be easily monitored;
	 * 
	 * @private
	 */
  async __registerSpiders() {
    if (!this.isAPIServiceOn) {
      return;
    }
    let spiders = this.crawlerMgr.getSpiders();
    for (let spider of spiders) {
      try {
        await API.spider.ping(spider.key);
      } catch (e) {
        await API.spider.registerSpider(
          spider.key,
          spider.name,
          spider.type,
          spider.host
        );
        await API.spider.ping(spider.key);
      }
    }
  }

  /**
	 * __refreshQueues cleans the queues of each crawler and restarts
	 * the crawler;
	 * 
	 * @private
	 */
  async __refreshQueues() {
    if (!this.isAPIServiceOn) {
      return;
    }
    let spiders = this.crawlerMgr.getSpiders();
    for (let spider of spiders) {
      try {
        let crawledEntities;
        if (spider.type == "estates") {
          crawledEntities = await API.crawledEstate.getCrawledEntities(
            spider.key
          );
        } else {
          crawledEntities = await API.crawledEstateAd.getCrawledEntities(
            spider.key
          );
        }

        let urls = crawledEntities.map(crawledEntity => {
          return crawledEntity.url;
        });
        await spider.refreshQueue(urls);
      } catch (e) {
        logger.warn(`failed to refreshQueues for spider ${spider.key}`);
        logger.warn(e && e.stack);
      }
    }
  }

  /**
	 * __recrawlEntities checks the API for old crawled entities that need to be recrawled
	 * and adds them to the queue;
	 * 
	 * @private
	 */
  async __recrawlEntities() {
    if (!this.isAPIServiceOn) {
      return;
    }

    let spiders = this.crawlerMgr.getSpiders();
    for (let spider of spiders) {
      try {
        let crawledEntities;
        if (spider.type === "estates") {
          crawledEntities = await API.crawledEstate.getEntitiesToReCrawl(
            spider.key
          );
        } else {
          crawledEntities = await API.crawledEstateAd.getEntitiesToReCrawl(
            spider.key
          );
        }

        let urls = crawledEntities.map(crawledEntity => {
          return crawledEntity.url;
        });
        logger.info(
          `found ${urls.length} properties to recrawl for spider ${spider.key}`
        );
        urls.forEach(url => spider.reCrawl(url));
      } catch (e) {
        logger.warn(`failed to recrawl properties for spider ${spider.key}`);
        logger.warn(e && e.stack);
      }
    }
  }

  /**
	 * __initLifecycleStreams initializes the lifecycle streams that will handle
	 * the restart of crawlers, reparsing of the old properties and so on;
	 * 
	 * @private
	 */
  __initLifecycleStreams() {
    // stream that will restart the crawlers
    // at the configured interval
    this.crawlersRestartStream = Rx.Observable
      .interval(RESTART_INTERVAL)
      .flatMap(() => this.__refreshQueues())
      .share();

    // stream that will recrawl the old properties once again
    this.refetchPropertiesStream = Rx.Observable
      .interval(RECRAWL_INTERVAL)
      .flatMap(() => this.__recrawlEntities())
      .share();

    // stream that will check the API Service availability
    this.checkAPIStream = Rx.Observable
      .interval(CHECK_API_INTERVAL)
      .share()
      .flatMap(() => API.heartbeat())
      .do(() => (this.isAPIServiceOn = true));

    // stream that will inform the API Service for the spider availability
    this.spiderHeartbeatStream = Rx.Observable
      .interval(SPIDERS_HB_INTERVAL)
      .filter(() => this.isAPIServiceOn)
      .flatMap(() => this.crawlerMgr.getSpiders())
      .flatMap(spider => API.spider.ping(spider.key))
      .share();
  }

  /**
	 * __startLifecycleStreams listens to the lifecycle streams and logs
	 * the results;
	 */
  __startLifecycleStreams() {
    this.crawlersRestartStream.subscribe(
      () => logger.info("crawlers restarted"),
      err => logger.warn(`failed to restart crawlers: ${err.stack}`)
    );

    this.refetchPropertiesStream.subscribe(
      () => logger.info("properties re-scheduled for crawling"),
      err =>
        logger.warn(
          `failed to re-schedule properties for crawling: ${err.stack}`
        )
    );

    this.checkAPIStream.subscribe(() => {},
    () => (this.isAPIServiceOn = false));

    this.spiderHeartbeatStream.subscribe(() => {}, () => {});
  }

  /**
	 * __listenCrawler listens all the crawlers' streams and performs the needed operations
	 * to parse each property and save it to the storage layer;
	 * 
	 * @private
	 */
  __listenCrawler() {
    let streamsMap = this.crawlerMgr.getStreams();
    let startSignal = streamsMap.startSignal;
    let crawlStream = streamsMap.pageStream
      .do(data =>
        logger.info(
          `crawled page from ${data.spiderMeta.type} :: ${data.spiderMeta
            .key} :: ${data.queueItem.url}`
        )
      )
      // parse the page
      .flatMap(data => {
        let parsedPromise = this.parserMgr.parsePage(
          data.spiderMeta,
          data.queueItem,
          data.body
        );
        return parsedPromise
          .then(result => {
            data.parsed = result;
            return data;
          })
          .catch(err => {
            logger.warn(`failed to parse page ${data.queueItem.url}`);
            logger.warn(`error was: ${err.stack}`);
          });
      })
      .filter(data => !!data.parsed)
      .filter(data => !!data.parsed.isRelevant)
      .do(data =>
        logger.info(
          `parsed page from ${data.spiderMeta.key} :: ${data.queueItem.url}`
        )
      )
      // garbage collect all the unneeded properties;
      .map(data => {
        return _.pick(data, ["spiderMeta", "queueItem", "parsed"]);
      })
      // validate
      .flatMap(data => {
        let validationPromise = validateExtracted(
          data.parsed.extracted,
          data.spiderMeta.type,
          data.spiderMeta.key
        );
        return validationPromise
          .then(validationResult => {
            if (!validationResult.isValid) {
              logger.warn(
                `validation result for url ${data.queueItem
                  .url}:: ${validationResult.err}`
              );
            } else {
              data.isValid = true;
            }
            return data;
          })
          .catch(() => {});
      })
      .do(data =>
        logger.info(
          `validated page from ${data.queueItem.url} which ${data.isValid
            ? "is"
            : "is not"} valid`
        )
      )
      .filter(data => data.isValid)
      .share();

    let estateStream = crawlStream
      .filter(data => {
        return "estates" === data.spiderMeta.type;
      })
      // transform to Property instance
      .flatMap(data => {
        let propertyPromise = Property.fromJSON(data.parsed);
        return propertyPromise
          .then(property => {
            data.property = property;
            return data;
          })
          .catch(() => {});
      })
      .filter(data => !!data.property)
      // save to API
      .flatMap(data => {
        if (!this.isAPIServiceOn) {
          data.savedSuccess = false;
          return Promise.resolve(data);
        }

        return API.crawledEstate
          .save(data.property)
          .then(() => {
            data.savedSuccess = true;
            return data;
          })
          .catch(err => {
            logger.warn(
              `failed to save property ${data.queueItem.url} to apiService;`
            );
            logger.warn(`error given: ${err.stack}`);
            return data;
          });
      })
      .flatMap(data => {
        // save to database, if API failed
        if (data.savedSuccess === true) {
          return Promise.resolve(data);
        }
        return PropertyStore.save(data.property)
          .then(() => data)
          .catch(() => data);
      });

    let estateAdStream = crawlStream
      .filter(data => {
        return "estate_ads" === data.spiderMeta.type;
      })
      // transform to EstateAd instance
      .flatMap(data => {
        let estateAdPromise = EstateAd.fromJSON(data.parsed);
        return estateAdPromise
          .then(estateAd => {
            data.estateAd = estateAd;
            return data;
          })
          .catch(() => data);
      })
      .filter(data => !!data.estateAd)
      // save to API
      .flatMap(data => {
        if (!this.isAPIServiceOn) {
          data.savedSuccess = false;
          return Promise.resolve(data);
        }

        return API.crawledEstateAd
          .save(data.estateAd)
          .then(() => {
            data.savedSuccess = true;
            return data;
          })
          .catch(err => {
            logger.warn(
              `failed to save estate ad ${data.queueItem.url} to apiService;`
            );
            logger.warn(`error given: ${err.stack}`);
            throw err;
            return data;
          });
      })
      // save to database, if API failed
      .flatMap(data => {
        if (data.savedSuccess === true) {
          return Promise.resolve(data);
        }
        return EstateAdStore.save(data.estateAd)
          .then(() => data)
          .catch(() => data);
      });

    let completeSignal = streamsMap.completeSignal;

    startSignal.subscribe(data =>
      logger.info(`crawler::${data.spiderMeta.key} started`)
    );

    estateStream.subscribe(
      data =>
        logger.info(
          `property :: ${data.queueItem.url} :: from crawler :: ${data
            .spiderMeta.key} successfully saved`
        ),
      err => logger.warn(`${err}`)
    );

    estateAdStream.subscribe(
      data =>
        logger.info(
          `estate ad :: ${data.queueItem.url} :: from crawler :: ${data
            .spiderMeta.key} successfully saved`
        ),
      err => logger.warn(`${err}`)
    );

    completeSignal.subscribe(
      data => logger.info(`crawler:: ${data.spiderMeta.key} finished`),
      err => {}
    );
  }

  /**
	 * __init initializes the daemon and the operations it performs in order;
	 * -> init lifecycle streams -> listen to lifecycle streams -> listen to crawlers;
	 * 
	 * @private
	 */
  __init() {
    this.__initLifecycleStreams();
    this.__startLifecycleStreams();
    this.__listenCrawler();
  }

  constructor() {
    this.crawlerMgr = new CrawlerManager();
    this.parserMgr = new ParserManager();
    this.isAPIServiceOn = false;
    this.__init();
  }

  /**
	 * start the engines;
	 */
  async start() {
    try {
      await this.__configureApiService();
    } catch (e) {
      logger.warn("api service unavailable");
    }
    await this.crawlerMgr.startSpiders();
    return;
  }

  async stop() {
    await this.crawlerMgr.stopSpiders();
    return;
  }
}

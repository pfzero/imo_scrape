import defaultEstateValidate from "core/validators/estates/default";
import defaultEstateAdValidate from "core/validators/estate_ads/default";

/**
 * getAdValidatorByType returns the validator to be used
 * for the specific ads crawler;
 * 
 * @param  String   spiderKey
 * @return Function
 */
function getAdValidatorBySpider(spiderKey) {
  let fn = defaultEstateAdValidate;
  try {
    fn = require(`core/validators/estate_ads/${spiderKey}`).default;
  } catch (e) {}
  return fn;
}

/**
 * getEstateValidatorBySpider returns the validator to be used
 * for the specific estates crawler based on the given spider key;
 * 
 * @param  String   spiderKey
 * @return Function
 */
function getEstateValidatorBySpider(spiderKey) {
  let fn = defaultEstateValidate;
  try {
    fn = require(`core/validators/estates/${spiderKey}`).default;
  } catch (e) {}
  return fn;
}

/**
 * validate validates the given extracted entity based on the 
 * type of the entity (estate or estate_ad) and the given spiderKey
 * 
 * @param  Object  extracted  the extracted entity data
 * @param  String  type       the type of the entity(estate_ad or estate)
 * @param  String  spiderKey  the spider's unique identifier
 * 
 * @return Promise
 * @public
 */
export default function validate(extracted, type, spiderKey) {
  let validationFn;
  if (type === "estate_ads") {
    validationFn = getAdValidatorBySpider(spiderKey);
  } else {
    validationFn = getEstateValidatorBySpider(spiderKey);
  }

  return validationFn(extracted);
}

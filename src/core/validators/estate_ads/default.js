import Joi from "joi";

// ad_schema is the required shape of an estate ad that is going to be
// saved in database;
let ad_schema = {
  title: Joi.string().optional().allow(""),
  category: Joi.string().optional().allow(""),
  description: Joi.string().required(),

  price: Joi.number().optional(),
  priceCurrency: Joi.any().valid(["eur", "euro", "lei", "ron", ""]).optional(),
  priceIncludesVat: Joi.boolean().optional(),

  city: Joi.string().optional().allow(""),

  contactName: Joi.string().optional().allow(""),
  contactEmail: Joi.string().email().optional().allow(""),
  contactPhone: Joi.string().optional().allow("")
};

// validate takes estate ad data and returns a promise
// that will contain the isValid flag set to true if the
// estate data is valid; false otherwise;
export default function validate(extracted) {
  return new Promise(resolve => {
    Joi.validate(extracted, ad_schema, err => {
      return resolve({
        isValid: err === null,
        err: err
      });
    });
  });
}

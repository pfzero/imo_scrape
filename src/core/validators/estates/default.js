import Joi from "joi";

// property_shape is the required shape of a property that is going to be
// saved in database;
let property_schema = {
  title: Joi.string().required(),
  category: Joi.object()
    .keys({
      key: Joi.string().required(),
      label: Joi.string().required()
    })
    .required(),
  originalProductCode: Joi.string(),
  price: Joi.number().allow(null).allow(0).optional(),
  priceCurrency: Joi.any().valid(["eur", "euro", "lei", "ron", ""]).optional(),
  priceIsPrivate: Joi.boolean().optional(),
  priceIncludesVat: Joi.boolean().optional(),
  description: Joi.string().allow("").optional(),
  sellerAgentName: Joi.string().optional(),
  sellerAgentEmail: Joi.string().email().optional(),
  sellerAgentPhone: Joi.string().optional(),
  mapCoordinates: Joi.object()
    .keys({
      lat: Joi.number().required(),
      lng: Joi.number().required()
    })
    .required(),
  address: Joi.object().optional(),
  mainImg: Joi.string().allow("").optional(),
  images: Joi.array().items(Joi.string()).optional(),
  meta: Joi.object().allow().optional()
};

// validate takes estate data and returns a promise
// that will contain the isValid flag set to true if the
// estate data is valid; false otherwise;
export default function validate(extracted) {
  return new Promise(resolve => {
    Joi.validate(extracted, property_schema, err => {
      return resolve({
        isValid: err === null,
        err: err
      });
    });
  });
}

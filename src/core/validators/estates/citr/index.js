import Joi from "joi";

const properties_without_photo = [
  "recreational_land",
  "parking_lot",
  "building_land",
  "land_for_construction",
  "infield",
  "free_land",
  "farm",
  "warehouse"
];

// property_shape is the required shape of a property that is going to be
// saved in database;
let property_schema = {
  title: Joi.string().required(),
  category: Joi.object()
    .keys({
      key: Joi.string().required(),
      label: Joi.string().required()
    })
    .required(),
  originalProductCode: Joi.string(),
  price: Joi.number().allow(null).allow(0).optional(),
  priceCurrency: Joi.any().valid(["eur", "euro", "lei", "ron", ""]),
  priceIsPrivate: Joi.boolean().optional(),
  priceIncludesVat: Joi.boolean().optional(),
  description: Joi.string().allow(""),
  sellerAgentName: Joi.string().required(),
  sellerAgentEmail: Joi.string().email().optional(),
  sellerAgentPhone: Joi.string().required(),
  mapCoordinates: Joi.object()
    .keys({
      lat: Joi.number().required(),
      lng: Joi.number().required()
    })
    .required(),
  address: Joi.object().optional(),
  mainImg: Joi.string().allow("").optional(),
  images: Joi.array().items(Joi.string()).optional(),
  meta: Joi.object().allow().optional()
};

function extraValidateProperty(extracted) {
  // if it has an image and a price
  if (!!extracted.mainImg && !!extracted.price) {
    return true;
  }

  // if it has an image, doesn't have a price
  // because the price it's private
  if (!!extracted.mainImg && !extracted.price && extracted.priceIsPrivate) {
    return true;
  }

  // if property's type is accepted to not have a photo and
  // the property has a price
  if (
    properties_without_photo.indexOf(extracted.category.key) > -1 &&
    !!extracted.price
  ) {
    return true;
  }

  // accepted without photo with private price;
  if (
    properties_without_photo.indexOf(extracted.category.key) > -1 &&
    !extracted.price &&
    extracted.priceIsPrivate
  ) {
    return true;
  }
  return false;
}

export default function validate(extracted) {
  return new Promise(resolve => {
    Joi.validate(extracted, property_schema, err => {
      return resolve({
        isValid: err === null && extraValidateProperty(extracted),
        err: err || new Error(`extra validation failed`)
      });
    });
  });
}

import Crawler from "simplecrawler";
import DiskQueue from "core/crawlers/crawler_queue_disk";
import _ from "lodash";
import mkdirp from "mkdirp";

/**
 * BaseCrawler is the class that simplifies the creation of the crawler based on
 * general configurations; For creating custom crawlers extend this class;
 *
 * @param String host 	   the hostname to crawl
 * @param Object {opts}      list of options
 */
class BaseCrawler extends Crawler {
  /**
	 * __initialize configures the crawler
	 */
  __initialize(crawlerSettings) {
    // make sure the database used by the queue
    // exists; otherwise create it;
    if (!this.queueDbPath) {
      throw new Error("missing required parameter queueDbPath");
    } else {
      mkdirp.sync(this.queueDbPath);
    }

    this.queue = new DiskQueue(this.queueDbPath);

    this.addAllowanceRules(crawlerSettings.fetchConditions.allow);
    this.addForbiddenRules(crawlerSettings.fetchConditions.forbid);
  }

  constructor(host, crawlerSettings) {
    super(host);
    Object.keys(
      _.omit(crawlerSettings, ["fetchConditions", "host"])
    ).forEach(key => {
      this[key] = crawlerSettings[key];
    });

    this.__initialize(crawlerSettings);
  }

  /**
	 * addAllowanceRules adds rules for allowing fetching/downloading of pages
	 * the crawler discovers
	 * 
	 * @param Object rules dictionary of rules to allow
	 * @return void
	 */
  addAllowanceRules(rules) {
    Object.keys(rules).forEach(ruleKey => {
      switch (ruleKey) {
        case "contentTypes":
          rules.contentTypes.forEach(contentType => {
            this.addDownloadCondition(queueItem =>
              new RegExp(contentType).test(queueItem.stateData.contentType)
            );
          });
          break;
      }
    });
  }

  /**
	 * addForbiddeneRules is the opposite of addAllowanceRules
	 * 
	 * @param Object rules dictionary of rules to forbit
	 * @return void 
	 */
  addForbiddenRules(rules) {
    Object.keys(rules).forEach(ruleKey => {
      switch (ruleKey) {
        case "contentTypes":
          rules.contentTypes.forEach(contentType => {
            this.addDownloadCondition(
              queueItem =>
                !new RegExp(contentType).test(queueItem.stateData.contentType)
            );
          });
          break;
        case "extensions":
          rules.extensions.forEach(ext => {
            this.addFetchCondition(
              queueItem => !new RegExp(ext, "i").test(queueItem.path)
            );
          });
      }
    });
  }

  /**
	 * emit wrapper that debugs the crawler;
	 * @param {String} evtName event's name
	 * @param {...any} rest    parameters to emit
	 */
  emit(evtName, ...rest) {
    // check if debug mode
    if (!this.debug) {
      return super.emit(evtName, ...rest);
    }

    this.queue.countItems({ fetched: true }, (err, completeCount) => {
      if (err) {
        throw err;
      }

      this.queue.getLength((err, length) => {
        if (err) {
          throw err;
        }

        console.log(
          "fetched %d of %d — %d open requests, %d open listeners",
          completeCount,
          length,
          this._openRequests.length,
          this._openListeners
        );
      });
    });
    let queueItem = rest[0];
    console.log(
      evtName,
      queueItem ? queueItem.url ? queueItem.url : queueItem : null
    );
    super.emit(evtName, ...rest);
  }

  /**
	 * pause() pauses the crawler
	 * @param  {Boolean} force [description]
	 */
  pause(force = true) {
    super.stop(force);
  }

  /**
	 * stop() stops the crawler
	 * to file;
	 * @return {Promise<void>}
	 */
  stop(force = true) {
    this.queue.close();
    super.stop(force);
  }
}

export default BaseCrawler;

import level from "level";
import _ from "lodash";
import config from "config";
import Rx from "rxjs/Rx";

/**
 * Recursive function that compares immutable properties on two objects.
 * @private
 * @param {Object} a Source object that will be compared against
 * @param {Object} b Comparison object. The functions determines if all of this object's properties are the same on the first object.
 * @return {Boolean} Returns true if all of the properties on `b` matched a property on `a`. If not, it returns false.
 */
function compare(a, b) {
  var matches;

  for (var key in a) {
    if (a.hasOwnProperty(key)) {
      if (typeof a[key] === "object" && typeof b[key] === "object") {
        matches = compare(a[key], b[key]);
      } else if (matches === false) {
        return false;
      } else {
        matches = a[key] === b[key];
      }
    }
  }

  return matches;
}

/**
 * DiskQueue is a custom implementation of a queue for simplecrawler that
 * is backed by levelDB storage
 */
export default class DiskQueue {
  /**
     * __getValueStream returns an Observable of the db values;
     */
  __getValueStream() {
    return Rx.Observable.create(observer => {
      let dbStream = this.db.createValueStream();
      dbStream.on("data", queueItem => observer.next(queueItem));
      dbStream.on("error", thrownErr => observer.error(thrownErr));
      dbStream.on("close", () => observer.complete());
      dbStream.on("end", () => observer.complete());
    });
  }

  /**
     * __getKeyStream returns an observable of the db keys;
     */
  __getKeyStream() {
    return Rx.Observable.create(observer => {
      let dbStream = this.db.createKeyStream();
      dbStream.on("data", key => observer.next(key));
      dbStream.on("error", thrownErr => observer.error(thrownErr));
      dbStream.on("close", () => observer.complete());
      dbStream.on("end", () => observer.complete());
    });
  }

  constructor(storagePath) {
    this.db = level(storagePath, {
      valueEncoding: "json"
    });
    this._allowedStatistics = [
      "actualDataSize",
      "contentLength",
      "downloadTime",
      "requestLatency",
      "requestTime"
    ];
  }

  /**
     * stop closes the database;
     */
  close() {
    this.db.close();
  }

  /**
     * Checks if a URL already exists in the queue. Returns the number of occurences
     * of that URL.
     * @param {String} url                         URL to check the existence of in the queue
     * @param {FetchQueue~existsCallback} callback
     */
  exists(url, callback) {
    this.db.get(url, (err, result) => {
      if (err) {
        if (err.notFound) return callback(null, false);
        return callback(err);
      }
      return callback(null, true);
    });
  }

  /**
     * Adds an item to the queue
     * @param {QueueItem} queueItem             Queue item that is to be added to the queue
     * @param {Boolean} [force=false]           If true, the queue item will be added regardless of whether it already exists in the queue
     * @param {FetchQueue~addCallback} callback
     */
  add(queueItem, force, callback) {
    const addToQueue = () => {
      queueItem.id = queueItem.url;
      queueItem.status = "queued";
      queueItem.timestamp = Date.now();
      this.db.put(queueItem.url, queueItem, function(err, result) {
        callback(err, queueItem);
      });
    };
    this.exists(queueItem.url, function(err, exists) {
      if (err) {
        return callback(err);
      } else if (!exists) {
        return addToQueue();
      } else if (force) {
        return addToQueue();
      } else {
        var error = new Error("Resource already exists in queue!");
        error.code = "DUPLICATE";
        callback(error);
      }
    });
  }

  /**
     * Get a queue item by url
     * @param {String} url                    The url of the queue item in the queue
     * @param {FetchQueue~getCallback} callback
     */
  get(url, callback) {
    this.db.get(url, callback);
  }

  /**
     * clean removes all the urls not matching the
     * given list of urls;
     * @param  {Array}    keepUrls list of urls to keep
     * @param  {Function} callback callback fn
     */
  clean(keepUrls = [], callback) {
    let keyStream = this.__getKeyStream();
    keyStream
      .filter(key => keepUrls.indexOf(key) === -1)
      .flatMap(key => {
        return new Promise((resolve, reject) => {
          this.db.del(key, err => {
            if (err) {
              return reject(err);
            }
            return resolve();
          });
        }).catch(err => {
          // do nothing with the error;
          // make sure we complete the deletion of all the keys
          // probably log the error?
        });
      })
      .toPromise()
      .then(() => callback())
      .catch(err => callback(err));
  }

  /**
     * Updates a queue item in the queue.
     * @param {Number} id                          ID of the queue item that is to be updated
     * @param {Object} updates                     Object that will be deeply assigned (as in `Object.assign`) to the queue item. That means that nested objects will also be resursively assigned.
     * @param {FetchQueue~updateCallback} callback
     */
  update(url, updates, callback) {
    this.db.get(url, (err, result) => {
      if (err) return callback(new Error("No queueItem found with that URL"));
      _.merge(result, updates);
      this.db.put(url, result, err => {
        callback(err, result);
      });
    });
  }

  /**
     * Gets the first unfetched item in the queue
     * @param {FetchQueue~oldestUnfetchedItemCallback} callback
     */
  oldestUnfetchedItem(callback) {
    let minStream = this.__getValueStream().filter(
      queueItem => queueItem.status === "queued"
    );

    minStream
      .min((firstItem, secondItem) => {
        return firstItem.timestamp < secondItem.timestamp ? -1 : 1;
      })
      .subscribe(
        queueItem => callback(null, queueItem),
        err => callback(err),
        () => callback(null, null)
      );
  }

  /**
     * Gets the maximum value of a stateData property from all the items in the
     * queue. This means you can eg. get the maximum request time, download size
     * etc.
     * @param {String} statisticName            Can be any of the strings in {@link FetchQueue._allowedStatistics}
     * @param {FetchQueue~maxCallback} callback
     */
  max(statisticName, callback) {
    if (this._allowedStatistics.indexOf(statisticName) === -1) {
      return callback(new Error("Invalid statistic"));
    }

    let valueStream = this.__getValueStream();
    valueStream
      .filter(queueItem => !!queueItem.fetched)
      .max(
        (first, second) =>
          first.stateData[statisticName] - second.stateData[statisticName]
      )
      .subscribe(
        queueItem => callback(null, queueItem.stateData[statisticName]),
        err => callback(err)
      );
  }

  /**
     * Gets the minimum value of a stateData property from all the items in the
     * queue. This means you can eg. get the minimum request time, download size
     * etc.
     * @param {String} statisticName            Can be any of the strings in {@link FetchQueue._allowedStatistics}
     * @param {FetchQueue~minCallback} callback
     */
  min(statisticName, callback) {
    if (this._allowedStatistics.indexOf(statisticName) === -1) {
      return callback(new Error("Invalid statistic"));
    }

    let valueStream = this.__getValueStream();
    valueStream
      .filter(queueItem => !!queueItem.fetched)
      .min(
        (first, second) =>
          first.stateData[statisticName] - second.stateData[statisticName]
      )
      .subscribe(
        queueItem => callback(null, queueItem.stateData[statisticName]),
        err => callback(err)
      );
  }

  /**
     * Gets the average value of a stateData property from all the items in the
     * queue. This means you can eg. get the average request time, download size
     * etc.
     * @param {String} statisticName            Can be any of the strings in {@link FetchQueue._allowedStatistics}
     * @param {FetchQueue~avgCallback} callback
     */
  avg(statisticName, callback) {
    if (this._allowedStatistics.indexOf(statisticName) === -1) {
      return callback(new Error("Invalid statistic"));
    }

    let baseStream = this.__getValueStream()
      .filter(queueItem => !!queueItem.fetched)
      .publish();

    let countStream = baseStream.count();
    let sumStream = baseStream.reduce(
      (accumulated, cur) => accumulated + cur.stateData[statisticName],
      0
    );

    countStream
      .combineLatest(sumStream)
      .map(results => results[1] / results[0])
      .subscribe(avg => callback(null, avg), err => callback(err));

    // kick off the baseStream
    baseStream.connect();
  }

  /**
     * Counts the items in the queue that match a selector
     * @param {Object} comparator                      Comparator object used to filter items. Queue items that are counted need to match all the properties of this object.
     * @param {FetchQueue~countItemsCallback} callback
     */
  countItems(comparator, callback) {
    this.filterItems(comparator, (err, items) => {
      if (err) {
        return callback(err);
      }
      return callback(null, items.length);
    });
  }

  /**
     * Filters and returns the items in the queue that match a selector
     * @param {Object} comparator                       Comparator object used to filter items. Queue items that are returned need to match all the properties of this object.
     * @param {FetchQueue~filterItemsCallback} callback
     */
  filterItems(comparator, callback) {
    let items = [], sent = false, err = null;
    let sendResp = () => {
      if (sent) {
        return;
      }
      sent = true;
      callback(err, items);
    };

    this.__getValueStream()
      .filter(queueItem => compare(comparator, queueItem))
      .subscribe(queueItem => items.push(queueItem), e => (err = e), sendResp);
  }

  /**
     * Gets the total number of queue items in the queue
     * @param {FetchQueue~getLengthCallback} callback
     */
  getLength(callback) {
    this.__getKeyStream()
      .count()
      .subscribe(cnt => callback(null, cnt), err => callback(err));
  }

  freeze(filename, cb) {
    throw new Error("already freezed!");
  }
  defrost(filename, cb) {
    throw new Error("not supported!");
  }
}

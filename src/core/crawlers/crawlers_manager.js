import config from "config";
import EE from "events";
import Spider from "./spider";

const crawlers_list = config.crawlers;

/**
 * CrawlerManager will instantiate all the crawlers based on the
 * configuration file; It will use the Spider helper for autoloading
 * the crawler either custom or base crawler;
 */
class CrawlerManager {
  constructor() {
    this.spiders = [];
    this.streams = {};
    this.__setupSpiders();
  }

  /**
	 * __setupSpiders will configure the list of crawlers
	 * 
	 * @return void
	 */
  __setupSpiders() {
    let self = this;
    crawlers_list.forEach(crawlerMeta => {
      if (!crawlerMeta.enabled) {
        return;
      }
      let spider = new Spider(crawlerMeta);
      this.spiders.push(spider);
      this.__mergeStreams(spider);
    });
  }

  /**
	 * __mergeStreams merges all the streams from a spider to the
	 * streams repo;
	 * @param {Spider} spider 
	 */
  __mergeStreams(spider) {
    let streamsMap = spider.getStreams();
    Object.keys(streamsMap).forEach(key => {
      if (!this.streams[key]) {
        this.streams[key] = streamsMap[key];
        return;
      }
      this.streams[key] = this.streams[key].merge(streamsMap[key]);
    });
  }

  /**
	 * reCrawl() recrawls the given url
	 * @param  {Object} item object containing the spider and the url
	 * @return {Promise<void>}
	 */
  reCrawl(item) {
    return this.getSpider(item.key).reCrawl(item.url);
  }

  /**
	 * startSpiders() starts all the spiders
	 * @return void
	 */
  async startSpiders() {
    for (let spider of this.spiders) {
      await spider.startCrawler();
    }
    return;
  }

  /**
	 * stopSpiders() will correctly stop the running crawlers
	 * @return {void}
	 */
  async stopSpiders() {
    for (let spider of this.spiders) {
      await spider.stopCrawler();
    }
    return;
  }

  /**
	 * getSpider() by the given key;
	 * 
	 * @param  {String} key 
	 * @return {Spider}     
	 */
  getSpider(key) {
    return this.spiders.find(spider => spider.key === key);
  }

  /**
	 * @return Array list of spiders
	 */
  getSpiders() {
    return this.spiders;
  }

  /**
	 * getStreams returns the repo of streams;
	 */
  getStreams() {
    return this.streams;
  }
}

export default CrawlerManager;

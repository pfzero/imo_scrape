import config from "config";
import EE from "events";
import _ from "lodash";
import logger from "winston";
import path from "path";
import BaseCrawler from "core/crawlers/base_crawler";
import Rx from "rxjs/Rx";

const QUEUE_DIR = config.crawler_general_settings.queueDir;

/**
 * Spider provides an easy way of instantiating and running
 * crawlers based on the configuration; The spider will autoload
 * custom crawlers and, if absent, will use the base crawler;
 *
 * @param Object crawlerMeta          the crawler configuration
 * @param String crawlerMeta.name     the crawler's name
 * @param String crawlerMeta.key      crawler's unique key
 * @param Bool   crawlerMeta.enabled  this flag indicates wether to run crawler
 * @param String crawlerMeta.host 	  crawler hostname to crawl
 * @param Object crawlerMeta.settings crawler settings
 */
export default class Spider {
  constructor(crawlerMeta) {
    // spider metadata
    this.name = crawlerMeta.name;
    this.key = crawlerMeta.key;
    this.enabled = crawlerMeta.enabled;
    this.host = crawlerMeta.host;
    this.type = crawlerMeta.type;

    // spider configurations
    let crawlerSettings = _.omit(crawlerMeta.settings, ["relevantPages"]);
    crawlerSettings.queueDbPath = path.join(
      global.APP_PATH,
      QUEUE_DIR,
      `${this.key}.db`
    );
    crawlerSettings.debug = crawlerMeta.debug;
    this.settings = _.merge(config.crawler_general_settings, crawlerSettings);

    // set relevant pages to emit after fetching
    this.relevantPages = crawlerMeta.settings.relevantPages || [];
    if (this.relevantPages.length === 0) {
      logger.warn(`No relevant pages were set for the crawler ${this.name}`);
      logger.warn(
        `This means that all pages will be treated as a valid product page`
      );
    }

    // initialize
    this.__initializeCrawler();
    this.__setupStreams();
    this.__connectStreams();
  }

  /**
	 * __initializeCrawlers will autoload the crawler
	 * @private
	 * @return void
	 */
  __initializeCrawler() {
    let CrawlerFactory = BaseCrawler;
    try {
      CrawlerFactory = require(`./custom/${this.key}_crawler`);
      if (!!CrawlerFactory.default) {
        CrawlerFactory = CrawlerFactory.default;
      } else {
        logger.error(
          `custom crawler for crawler: ${this.key} is incorrect. Check if it's a class!`
        );
      }
    } catch (e) {}
    this.crawler = new CrawlerFactory(this.host, this.settings);
  }

  /**
	 * @private
	 * @return Object the spiderMeta to be sent
	 */
  __getSpiderMeta() {
    return {
      name: this.name,
      key: this.key,
      host: this.host,
      type: this.type
    };
  }

  /**
	 * __setupStreams will setup the streams emitted by the crawler
	 * 
	 * @private
	 */
  __setupStreams() {
    this.startSignal = Rx.Observable
      .fromEvent(this.crawler, "crawlstart", () => {
        return { spiderMeta: this.__getSpiderMeta() };
      })
      .publish();

    this.completeSignal = Rx.Observable
      .fromEvent(this.crawler, "complete", () => {
        return { spiderMeta: this.__getSpiderMeta() };
      })
      .publish();

    this.pageStream = Rx.Observable
      .fromEvent(this.crawler, "fetchcomplete", (queueItem, body, resp) => {
        return {
          spiderMeta: this.__getSpiderMeta(),
          queueItem,
          body,
          resp
        };
      })
      .filter(item => {
        let { queueItem } = item;
        let isRelevantPage = false;
        this.relevantPages.forEach(path => {
          isRelevantPage =
            isRelevantPage || new RegExp(path).test(queueItem.url);
        });
        return isRelevantPage;
      })
      .publish();

    this.pageNotFoundStream = Rx.Observable
      .fromEvent(this.crawler, "fetch404", queueItem => {
        return { spiderMeta: this.__getSpiderMeta(), queueItem };
      })
      .publish();
  }

  /**
	 * __connectStreams connects the existing streams and their
	 * subscribers to the original source of data;
	 * 
	 * @private
	 */
  __connectStreams() {
    Object.keys(this.getStreams()).forEach(streamKey => {
      let stream = this[streamKey];
      if (typeof stream.connect === "function") {
        stream.connect();
      }
    });
  }

  /**
	 * getStreams() returns all the underlying streams;
	 */
  getStreams() {
    return {
      startSignal: this.startSignal,
      completeSignal: this.completeSignal,
      pageStream: this.pageStream,
      pageNotFoundStream: this.pageNotFoundStream
    };
  }

  /**
	 * getCrawler() returns the underlying crawler
	 * 
	 * @return Crawler the underlying crawler
	 */
  getCrawler() {
    return this.crawler;
  }

  /**
	 * refreshQueue() restarts the crawling:
	 * 	1. it removes all the items from the queue,
	 * 	2. it adds all the crawled properties to queue and marks those
	 * 		as downloaded;
	 * 	3. it restarts the crawler();
	 * @return {Promise<void>} 
	 */
  async refreshQueue(keepURLs = []) {
    if (!this.enabled) {
      return;
    }
    this.crawler.pause();
    let queue = this.crawler.queue;
    let resolve, reject;
    let retPromise = new Promise((res, rej) => {
      resolve = res;
      reject = rej;
    });
    queue.clean(keepURLs, () => {
      this.crawler.start();
      resolve();
    });
    return retPromise;
  }

  /**
	 * reCrawl() recrawls the given url;
	 * @param  {String} url
	 * @return {void}     
	 */
  reCrawl(url) {
    this.crawler.queueURL(url, undefined, true);
  }

  /**
	 * startCrawler starts the underlying crawler only if the
	 * enabled flag is true
	 * 
	 * @return Promise<void>
	 */
  async startCrawler() {
    if (this.enabled) {
      return await this.crawler.start();
    }
    return;
  }

  /**
	 * Stops the crawler;
	 * 
	 * @return Promise<void>
	 */
  async stopCrawler() {
    if (this.enabled) {
      return await this.crawler.stop();
    }
    return;
  }
}

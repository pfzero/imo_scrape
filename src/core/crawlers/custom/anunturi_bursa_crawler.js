import cheerio from "cheerio";
import BaseCrawler from "core/crawlers/base_crawler";

export default class AnunturiBursaCrawler extends BaseCrawler {
  __initialize(...args) {
    super.__initialize(...args);

    // mega hack;
    this.on("fetchheaders", (queueItem, response) => {
      // now, we have to modify the response's status code
      // to 200 because the webpage we are crawling is full of shit;
      // thus, will prevent the crawler from stopping;
      if (response.statusCode === 404) {
        response.statusCode = 200;
      }
    });
  }

  // also overwrite this method because the
  // links are fucked up too;
  discoverResources(buffer, queueItem) {
    var $ = cheerio.load(buffer.toString("utf8"));
    let links = $("a[href]")
      .map(function() {
        return $(this).attr("href");
      })
      .get();

    // we need this hack to get rid of the \n\t\t\t\t\t\t
    // sequence from the link address;
    return links.map(link => link.replace(/\n*\t*/gi, ""));
  }
}

import config from "config";
import logger from "winston";
import BaseParser from "core/parsers/base_parser";
import _ from "lodash";

/**
 * ParsersManager handles parsers and knows which page to dispatch
 * to which parser; e.g. parse page from sales.somedomain to 
 * sales.somedomain parser;
 * 
 * Also, it will autoload all the parsers based on the crawlers property
 * from the configuration file; Each enabled crawler will have a 1:1
 * mapping with a parser;
 */
export default class ParsersManager {
  constructor() {
    this.parsers = {};
    this.__setupParsers();
  }

  /**
	 * __loadParser loads the parser for the given key; It will search for the
	 * parser custom class in core/parsers/custom/${parserKey}; If the custom
	 * class is not found, it will use the base parser class; Then, the parser
	 * configuration will be loaded from custom/parsers/parsers_configs/${parserKey}
	 * to pass when instantiating the parser;
	 *
	 * @private
	 * @param  {ParserConfig} key parser unique key
	 * @return {Parser}
	 */
  __loadParser(parser) {
    let ParserFactory;
    let parserConfigs;

    try {
      ParserFactory = require(`core/parsers/custom/${parser.customParserName}`)
        .default;
    } catch (e) {
      logger.info(`using base parser for host ${parser.key}`);
      ParserFactory = BaseParser;
    }

    try {
      parserConfigs = require(`core/parsers/parsers_configs/${parser.type}/${parser.key}`)
        .default;
    } catch (e) {
      logger.error(`No parser configuration found for ${parser.key}`);
      logger.error(`Cannot continue bootstrapping the app without it`);
      throw e;
    }
    return new ParserFactory(parserConfigs);
  }

  /**
	 * __setupParsers will instantiate all the parsers for which there is an enabled
	 * crawler in configuration file;
	 *
	 * @private
	 * @return {void}
	 */
  __setupParsers() {
    let enabledParsers = config.crawlers.filter(crawler => crawler.enabled);
    enabledParsers.forEach(parser => {
      this.parsers[parser.key] = this.__loadParser(parser);
    });
  }

  /**
	 * parsePage will parse the given page;
	 * 
	 * @param  {Object} spiderMeta spider meta from which this page originates
	 * @param  {Object} queueItem  item found in spider's queue
	 * @param  {String} page       the html to parse
	 * @return {Object} parsedPage the parsed page;
	 */
  async parsePage(spiderMeta, queueItem, page) {
    let parser = this.parsers[spiderMeta.key];
    if (!parser) {
      throw new Error(`No parser available for ${spiderMeta.key}`);
    }
    let parsedPage = await parser.parsePage(page);
    return _.merge(parsedPage, {
      spider: spiderMeta.key,
      url: queueItem.url
    });
  }
}

import cheerio from "cheerio";
import logger from "winston";
import _ from "lodash";
import { StringDecoder } from "string_decoder";
import { trimObjectValues } from "utils";

/**
 * BaseParser is the base class that can handle parsing of a webpage
 * based on the specific parser configuration; The parsing strategy
 * is to collect all the properties from the given parser configuration
 * object and call the parse method for each property;
 *
 * The parserConfiguration object shape is:
 * 	{
 * 		key: 'parser_id', // e.g. bcr
 * 		
 * 		// preParse logic;
 * 		preParse: async fn(pageMeta),
 * 		postParse: async fn(pageMeta),
 * 		extract: {
 * 			property: {
 * 				
 * 				// the selector where the property's value
 * 				// can be found
 * 				selector: 'body div.title',
 * 				
 * 				// parser logic on the given element;
 * 				// function which will be called with $el queryed
 * 				// by the above selector, $html (jQuery wrapped rawHTML),
 * 				// rawHTML and extracted (the object containing previously
 * 				// extracted properties)
 * 				// this function must return Promise<parsedValue>
 * 				parse: async fn({$el, $html, rawHTML, extracted})
 * 			},
 * 			// e.g.
 * 			title: {
 * 				selector: 'h1.title',
 * 				parse: async ({ $el, $html, extracted }) => {
 * 					return $el.text();
 * 				}
 * 			}
 * 		}
 * 	}
 * 	@see core/parsers/parsers_configs for examples of parserConfiguration object;
 * 	@param {Object} parserConfiguration the parser configuration object;
 */
export default class BaseParser {
  constructor(parserConfiguration) {
    this.parserConf = parserConfiguration;
    this.properties = Object.keys(this.parserConf.extract);
  }

  /**
	 * __prepare takes the raw html string and creates the 
	 * pageMeta object; This object is initialized with default
	 * values;
	 *
	 * @private
	 * @param  {Buffer} html 		the raw html string
	 * @return {Object} pageMeta    the pageMeta object
	 */
  __prepare(html) {
    let prepared = html;
    if (typeof this.parserConf.prepare === "function") {
      prepared = this.parserConf.prepare(html);
    }

    return {
      rawHTML: prepared,
      $html: cheerio.load(prepared),
      parsed: {
        extracted: {},
        isRelevant: true,
        isNotFound: false,
        isSuccessfullParse: true
      }
    };
  }

  /**
	 * __preParse logic; This is the place where the page can be
	 * additionaly prepared for parsing or deciding wether the page
	 * is worth parsing; If the returned pageMeta object has the
	 * isRelevant flag set to false, then the page will not be parsed;
	 *
	 * @private
	 * @param  {Object} pageMeta the pageMeta object created using __prepare()
	 * @return {Object} pageMeta
	 */
  async __preParse(pageMeta) {
    if (typeof this.parserConf.preParse === "function") {
      return this.parserConf.preParse(pageMeta);
    }
    return pageMeta;
  }

  /**
	 * __parse does the actual parsing of the html page; This is the place
	 * where all the properties will be parsed from html and turned into a json
	 * object;
	 *
	 * @private
	 * @param  {Object} pageMeta
	 * @return {Object} pageMeta
	 */
  async __parse(pageMeta) {
    const $html = pageMeta.$html;
    let extracted = pageMeta.parsed.extracted;
    for (let property of this.properties) {
      let propParser = this.parserConf.extract[property];
      let $el = $html(propParser.selector);
      let input = { $html, $el, rawHTML: pageMeta.rawHTML, extracted };
      try {
        extracted[property] = await propParser.parse(input);
      } catch (e) {
        extracted[property] = null;
      }
    }
    return pageMeta;
  }

  /**
	 * __postParse handles additional post parsing logic;
	 *
	 * @private
	 * @param  {Object} pageMeta
	 * @return {Object} pageMeta
	 */
  async __postParse(pageMeta) {
    let postParsed;
    if (typeof this.parserConf.postParse === "function") {
      postParsed = this.parserConf.postParse(pageMeta);
    } else {
      postParsed = pageMeta;
    }
    postParsed.parsed.extracted = trimObjectValues(postParsed.parsed.extracted);
    return postParsed;
  }

  /**
	 * parsePage parses a raw html string into
	 * a structured json object;
	 *
	 * @public
	 * @param  {String} rawHTML 		 the raw html to parse
	 * @return {Object} pageMeta.parsed  
	 * @see __prepare().parsed to see the returned object;
	 */
  async parsePage(rawHTML) {
    let pageMeta = this.__prepare(rawHTML);
    let parsed = pageMeta.parsed;
    try {
      pageMeta = await this.__preParse(pageMeta);
      if (!pageMeta.parsed.isRelevant) {
        return parsed;
      }
      pageMeta = await this.__parse(pageMeta);
      if (!pageMeta.parsed.isRelevant) {
        return parsed;
      }
      pageMeta = await this.__postParse(pageMeta);
    } catch (e) {
      logger.warn(`couldn't parse page for ${this.parserConf.key}`);
      logger.warn(e.stack);
      // make sure to mark this page as not parsed;
      parsed.isSuccessfullParse = false;
    }
    return parsed;
  }
}

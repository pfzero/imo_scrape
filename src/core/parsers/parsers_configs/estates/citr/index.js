import vm from "vm";
import cheerio from "cheerio";
import url from "url";
import numeral from "numeral";
import logger from "winston";
import { format, parse } from "libphonenumber-js";

import categories, { getCategory } from "core/resources/estates/categories";
import categoriesMapping from "./categories_mapping";
import { getCategoryKey } from "utils";
import blackListedImages from "./image_blacklist";
import extraPropsMappings from "./extra_props_mapping";
import { ExtraPropsSchema } from "./extra_props_mapping";

let getOriginalImg = imageURL => {
  let parsedURL = url.parse(imageURL, true);
  delete parsedURL.query.size;
  return url.format(parsedURL);
};

let isImageBlacklisted = imageURL => {
  for (var i = 0; i < blackListedImages.length; i++) {
    let pattern = blackListedImages[i];
    if (imageURL.indexOf(pattern) > -1) {
      return true;
    }
  }
  return false;
};

let extractProperty = row => {
  let cols = cheerio(row).find("td");
  let extractedProperty = {};
  let label = cheerio(cols[0]);
  let value = cheerio(cols[1]);
  extractedProperty[label.text()] = value.text();
  return extractedProperty;
};

let parseExtraProperties = rows => {
  let extracted = {};

  rows.each((id, row) => {
    let parsed = extractProperty(row);
    Object.keys(parsed).forEach(key => (extracted[key] = parsed[key]));
  });

  let parsed = {};
  Object.keys(extracted).forEach(key => {
    let trimmedKey = key.trim().toLowerCase();
    let trimmedVal = extracted[key].trim().toLowerCase();
    let availableMappings = Object.keys(extraPropsMappings);
    let collected = {};
    for (var i = 0; i < availableMappings.length; i++) {
      let k = availableMappings[i];
      if (trimmedKey.indexOf(k) > -1) {
        if (trimmedKey.indexOf("(ha)") > -1) {
          trimmedVal = +trimmedVal;
          collected[extraPropsMappings[k]] = trimmedVal * 10000;
          break;
        }
        if (typeof trimmedVal === "string") {
          let numberVal = parseFloat(trimmedVal, 10);
          trimmedVal = isNaN(numberVal) ? trimmedVal : numberVal;
        }
        collected[extraPropsMappings[k]] = trimmedVal === "da"
          ? true
          : trimmedVal === "nu" ? false : trimmedVal;
      }

      if (typeof collected.balcony !== "boolean") {
        delete collected.balcony;
      }
      if (typeof collected.terrace !== "boolean") {
        delete collected.terrace;
      }

      if (typeof trimmedVal === "string" && trimmedVal.indexOf(k) > -1) {
        collected[extraPropsMappings[k]] = true;
        break;
      }
    }
    let collectedKeys = Object.keys(collected);
    if (collectedKeys.length === 0) {
      logger.warn(`undefined mapping for extra property: ${trimmedKey}`);
      return;
    }
    collectedKeys.forEach(k => (parsed[k] = collected[k]));
  });
  return new ExtraPropsSchema(parsed);
};

// citr_config is the configuration file
// needed for parsing pages crawled by the 'citr'
// crawler (from config settings)
let citr_config = {
  key: "citr",
  // preparse logic
  preParse: async function(pageMeta) {
    const { parsed, $html } = pageMeta;
    const $invalidProd = $html("div.produsInvalid");
    // check for this error message...
    // if present, we have an invalid product;
    // in this case it's the same with not_found product;
    if (
      $invalidProd &&
      $invalidProd.text().toLowerCase() === "produs invalid"
    ) {
      parsed.isRelevant = false;
      parsed.isNotFound = true;
      return pageMeta;
    }
    // check for product categories;
    const $categories = $html(".BreadcrumbsList li.Main a");
    // if no categories are present, then we are looking for a
    // page that's not relevant for us;
    if ($categories === null) {
      parsed.isRelevant = false;
    }
    // if we don't have a registered category for the product's category,
    // we are done;
    let categoryKey = await citr_config.extract.category.parse({
      $el: $categories
    });
    if (!categoryKey) {
      parsed.isRelevant = false;
    }
    return pageMeta;
  },
  extract: {
    title: {
      selector: ".CITRPortalDetaliuProdus h1",
      parse: async function({ $el }) {
        return $el.text();
      }
    },
    category: {
      selector: ".BreadcrumbsList li.Main a",
      parse: async function({ $el }) {
        let $categ = $el.last();
        if ($categ === null) {
          return null;
        }
        let key = getCategoryKey(categoriesMapping, $categ.text());
        return getCategory(key);
      }
    },
    originalProductCode: {
      selector: ".localizare .judet",
      parse: async function({ $el }) {
        let matches = $el.text().match(/cod produs: ([0-9]+)/i);
        if (matches === null) {
          return "";
        }
        return matches[1];
      }
    },
    price: {
      selector: ".priceContainer .pretProdus span:first-child",
      parse: async function({ $el }) {
        if (!$el) {
          return "";
        }
        return numeral($el.text().replace(/\s+/gi, "")).value();
      }
    },
    priceCurrency: {
      selector: ".pretProdus span:nth-child(2)",
      parse: async function({ $el }) {
        if (!$el) {
          return "";
        }
        return $el.text().toLowerCase();
      }
    },
    priceIncludesVat: {
      selector: ".priceContainer > span",
      parse: async function({ $el }) {
        if (!$el) {
          return true;
        }
        return $el.text().toLowerCase() === "pretul nu include tva"
          ? false
          : true;
      }
    },
    priceIsPrivate: {
      selector: ".priceContainer > span.pretIndisponibil",
      parse: async function({ $el }) {
        if (!$el) {
          return false;
        }
        return $el.text().toLowerCase().length > 0;
      }
    },
    description: {
      selector: "p.description",
      parse: async function({ $el }) {
        return $el ? $el.text() : "";
      }
    },
    sellerAgentName: {
      selector: ".dateContact p.nume",
      parse: async function({ $el }) {
        return $el ? $el.text() : "";
      }
    },
    sellerAgentEmail: {
      selector: ".dateContact > span > a",
      parse: async function({ $el }) {
        return $el ? $el.text() : "";
      }
    },
    sellerAgentPhone: {
      selector: ".dateContact > span",
      parse: async function({ $el }) {
        if (!$el) {
          return "";
        }
        let parsed = parse($el.text(), "RO");
        return format(parsed, "International_plaintext");
      }
    },
    mapCoordinates: {
      selector: "body script",
      parse: async function({ $el }) {
        if (!$el) {
          return null;
        }
        let sandbox = {
          SystemCore: {
            createUIApplication: () => ({})
          }
        };
        $el.each((idx, script) => {
          let scriptTxt = cheerio.load(script).text();
          scriptTxt = `try { ${scriptTxt} } catch(e) {}`;
          let scriptToRun = new vm.Script(scriptTxt);
          scriptToRun.runInNewContext(sandbox);
        });
        if (!sandbox.props) {
          return null;
        }
        let latLng = sandbox.props.latLong.split(",");
        let lat = parseFloat(latLng[0], 10);
        let lng = parseFloat(latLng[1], 10);
        return { lat, lng };
      }
    },
    mainImg: {
      selector: ".mainImageContainer > a",
      parse: async function({ $el }) {
        if (!$el || !$el.attr("href")) {
          return "";
        }

        let imgSrc = $el.attr("href");
        return isImageBlacklisted(imgSrc) ? "" : getOriginalImg(imgSrc);
      }
    },
    images: {
      selector: ".productImages .thumbContainer a",
      parse: async function({ $el }) {
        if (!$el) {
          return [];
        }
        let images = [];
        $el.each((idx, anchor) => {
          let $a = cheerio(anchor);
          let imgSrc = $a.attr("href");
          if (!imgSrc) {
            return;
          }
          if (isImageBlacklisted(imgSrc)) {
            return;
          }
          images.push(getOriginalImg(imgSrc));
        });
        return images;
      }
    },
    meta: {
      selector: ".specificatii table tr",
      parse: async function({ $el }) {
        if (!$el) {
          return;
        }
        let parsed;
        try {
          parsed = parseExtraProperties($el);
        } catch (e) {
          console.error(e);
        }
        return parsed;
      }
    }
  }
};

export default citr_config;

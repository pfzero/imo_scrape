// citr categories mapping
export default {
  Rezidentiale: "residential",
  Apartamente: "apartment",
  "Cabane, spatii agrement": "recreational_land",
  "Case, vile": "house",
  "Loc de parcare": "parking_lot",
  Garaj: "garage",
  "Boxa, spatiu depozitare": "storage_space",
  "Spatii, centre comerciale": "commercial",
  "Centre comerciale": "commercial_center",
  "Spatii comerciale": "commercial_space",
  Terenuri: "land",
  "Teren cu constructii": "building_land",
  "Terenuri agricole": "infield",
  "Terenuri libere": "free_land",
  "Terenuri pentru constructii": "land_for_construction",
  Birouri: "office_building",
  farm: "Ferme",
  "Hoteluri, Pensiuni": "hotel",
  "Spatii productie, depozitare": "warehouse",
  "Statii distributie carburanti": "petrol_station",
  "Unitati de productie": "industrial_unit"
};

import { format, parse } from "libphonenumber-js";
import { Iconv } from "iconv";

let anunturi_bursa_config = {
  key: "anunturi_bursa",

  prepare: htmlBuf => {
    let transformer = new Iconv("ISO-8859-2", "UTF-8");
    let converted = transformer.convert(htmlBuf);
    return converted;
  },

  preParse: async function(pageMeta) {
    const { $html, parsed } = pageMeta;
    let $primaryCategory = $html(
      "span.albastru_sectiune_navigatie a:nth-child(1)"
    );
    let $action = $html("span.albastru_sectiune_navigatie a:nth-child(2)");

    let primaryText =
      ($primaryCategory && $primaryCategory.text().toLowerCase()) || "";
    let actionText = ($action && $action.text().toLowerCase()) || "";

    if (!(primaryText === "diverse" || primaryText === "imobiliare")) {
      parsed.isRelevant = false;
    }

    if (!(actionText === "vânzări" || actionText === "licitaţii")) {
      parsed.isRelevant = false;
    }

    return pageMeta;
  },

  extract: {
    description: {
      selector: ".caseta_sectiuni_anunt_text",
      parse: async function({ $el }) {
        if (!$el) {
          return;
        }
        return $el.text().trim().replace(/\n+\t+/gi, "");
      }
    },
    category: {
      selector: "span.albastru_sectiune_navigatie a:nth-child(3)",
      parse: async function({ $el }) {
        if (!$el) {
          return;
        }
        console.log("category: ", $el.text().toLowerCase());
        return $el.text().toLowerCase();
      }
    },
    contactPhone: {
      selector: ".caseta_sectiuni_anunt_text",
      parse: async function({ $el }) {
        if (!$el) {
          return;
        }
        let text = $el.text().trim();
        let regex = /tel\..+$/gi;
        let matches = text.match(regex);
        if (matches.length == 0) {
          return;
        }
        let phoneText = matches[0];
        let parsed = parse(phoneText, "RO");
        return format(parsed, "International_plaintext");
      }
    }
  }
};

export default anunturi_bursa_config;

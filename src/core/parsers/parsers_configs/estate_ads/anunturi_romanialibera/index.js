import { format, parse } from "libphonenumber-js";
import numeral from "numeral";
import cheerio from "cheerio";

let anunturi_romanialibera_config = {
  key: "anunturi_romanialibera",

  preParse: async function(pageMeta) {
    const { $html, parsed } = pageMeta;

    let links = $html("div#breadcrumb a");
    let category = links.filter(
      (k, el) => cheerio(el).text().toLowerCase() === "anunturi licitatii"
    );

    if (category.length === 0) {
      pageMeta.parsed.isRelevant = false;
    }

    return pageMeta;
  },

  extract: {
    title: {
      selector: "h1#titlu",
      parse: async function({ $el }) {
        if (!$el) {
          return;
        }
        return $el.text().trim();
      }
    },
    description: {
      selector: "h2#descriere",
      parse: async function({ $el }) {
        if (!$el) {
          return;
        }
        return $el.text().trim();
      }
    },
    price: {
      selector: "div.pret",
      parse: async function({ $el }) {
        if (!$el) {
          return;
        }
        let lastEl = $el.last();
        let text = lastEl.text().trim().replace(/[\.\s(?:RON)(?:EUR)]/g, "");
        return numeral(text).value();
      }
    },
    priceCurrency: {
      selector: "div.pret",
      parse: async function({ $el }) {
        if (!$el) {
          return;
        }
        let lastEl = $el.last();
        let text = lastEl.text().trim().replace(/[\.0-9\s]/g, "");
        return text.toLowerCase();
      }
    },
    city: {
      selector: "div#basic_info > div",
      parse: async function({ $el }) {
        if (!$el) {
          return;
        }
        let container = $el.first();
        let spanEl = cheerio(container).find("span").last();
        return spanEl.text().trim();
      }
    },
    contactPhone: {
      selector: "div#trimite_email_info",
      parse: async function({ $el }) {
        if (!$el) {
          return;
        }
        let phoneText = $el.text().trim();
        let parsed = parse(phoneText, "RO");
        return format(parsed, "International_plaintext");
      }
    }
  }
};

export default anunturi_romanialibera_config;

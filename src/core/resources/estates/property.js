import geoService from "core/services/geo";
import Entity from "core/resources/entity";

/**
 * Property is the class containing the logic for a single
 * parsed property; It contains utility methods for handling
 * a property saved already in db or a property that was
 * recently crawled
 */
export default class Property extends Entity {
  /**
	 * _fillAdditionalMeta() generates the additional
	 * metadata fields that need to be computed on the
	 * given property; e.g. full address from map geocoordinates
	 * and createdAt fields;
	 * @return {Property}
	 * @private
	 */
  async __fillAdditionalMeta() {
    if (
      this.isRelevant &&
      !!this.extracted.mapCoordinates &&
      !this.extracted.address
    ) {
      await this.setAddressFromGeo();
    }
    if (
      this.isRelevant &&
      !!this.extracted.address &&
      !this.extracted.mapCoordinates
    ) {
      // todo
      // await this.setGeoFromAddress();
    }

    return this;
  }

  /**
	 * setAddressFromGeo reverse geocodes the map
	 * coordinates of the property and sets the address
	 * field;
	 */
  async setAddressFromGeo() {
    if (!this.extracted.mapCoordinates) {
      return;
    }
    this.extracted.address = await geoService.reverse(
      this.extracted.mapCoordinates
    );
  }

  /**
	 * fromJSON() generates a Property instance from a
	 * json object; typically the extracted object after
	 * crawling
     * 
	 * @param  {Object} crawledProperty
	 * @return {Property}                 
	 */
  static async fromJSON(crawledProperty) {
    let property = new Property(crawledProperty);
    await property.__fillAdditionalMeta();
    return property;
  }

  /**
	 * fromDB() generates a Property from the database
	 * serialized object;
     * 
	 * @param  {Object} dbProperty 
	 * @return {Promise<Property>}
	 */
  static async fromDB(dbProperty) {
    let entity = new Property(dbProperty);
    return entity;
  }

  /**
     * fromBackend instantiates the property with the values taken
     * from the API
     * @param {Object} apiProperty
	 * @return {Promise<Property>}
     */
  static async fromBackend(apiProperty) {
    return new Property(apiProperty, "api");
  }
}

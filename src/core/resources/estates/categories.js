const CATEGORIES = [
  {
    key: "residential",
    label: "Residential",
    children: [
      { key: "apartment", label: "Apartment" },
      { key: "chalet", label: "Chalet" },
      { key: "recreational_land", label: "Recreational Land" },
      { key: "house", label: "House" },
      { key: "parking_lot", label: "Parking Lot" },
      { key: "garage", label: "Garage" },
      { key: "storage_space", label: "Storage Space" }
    ]
  },
  {
    key: "commercial",
    label: "Commercial",
    children: [
      { key: "commercial_center", label: "Commercial Center" },
      { key: "commercial_space", label: "Commercial Space" }
    ]
  },
  {
    key: "land",
    label: "Land",
    children: [
      { key: "building_land", label: "Building Land" },
      { key: "land_for_construction", label: "Land For Construction" },
      { key: "infield", label: "Infield" },
      { key: "free_land", label: "Free Land" }
    ]
  },
  {
    key: "office_building",
    label: "Office"
  },
  {
    key: "farm",
    label: "Farm"
  },
  {
    key: "hotel",
    label: "Hotel"
  },
  {
    key: "pension",
    label: "Pension"
  },
  {
    key: "fabric",
    label: "Fabric"
  },
  {
    key: "warehouse",
    label: "Warehouse"
  },
  {
    key: "petrol_station",
    label: "Petron Station"
  },
  {
    key: "industrial_unit",
    label: "Industrial Unit"
  }
];

export default CATEGORIES;

export function getCategory(key, subcategs) {
  let foundCategory;
  let toSearch = subcategs || CATEGORIES;
  for (let i = toSearch.length - 1; i >= 0; i--) {
    let item = toSearch[i];
    if (item.key === key) {
      return item;
    }
    if (item.children instanceof Array) {
      let subItem = getCategory(key, item.children);
      if (subItem) {
        return subItem;
      }
    }
  }
}

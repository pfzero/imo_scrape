import Entity from "core/resources/entity";

export default class EstateAd extends Entity {
  static async fromJSON(crawledAd) {
    let estateAd = new EstateAd(crawledAd);
    return estateAd;
  }

  static async fromDB(dbEstateAd) {
    let estateAd = new EstateAd(dbEstateAd);
    return estateAd;
  }

  static async fromBackend(apiEstateAd) {
    return new EstateAd(apiEstateAd, "api");
  }
}

import _ from "lodash";
import etag from "etag";

/**
 * Entity is the class containing the logic for a single
 * parsed page; It contains utility methods for handling
 * the page metadata saved already in db or the meta obtained
 * after a recent crawling
 * 
 * @abstract
 */
export default class Entity {
  /**
	 * _getEtagFields() collects the relevant fields for generating
	 * the eTag of the entity; The etag is used to determine wether
	 * an entity has been changed after the last crawling compared
	 * to the one previously saved in database
	 * @return {Object} 
     * @protected
	 */
  _getEtagFields() {
    return this.extracted;
  }

  /**
     * _genEtag() computes the etag of the entity and returns it;
     * 
     * @return {string}
     */
  _genEtag() {
    return etag(JSON.stringify(this._getEtagFields()));
  }

  /**
     * _fillAdditionalMeta() sets additional properties on the entity;
     * Use this to make sure fields are set correctly after the entity is
     * created after a page parse;
     * 
     * @protected
     */
  async _fillAdditionalMeta() {}

  constructor(crawledData, source = "db") {
    _.merge(this, crawledData);

    if (!!this.createdAt && !_.isDate(this.createdAt)) {
      this.createdAt = source === "db"
        ? new Date(JSON.parse(this.createdAt))
        : new Date(this.createdAt);
    }
    if (!!this.updatedAt && !_.isDate(this.updatedAt)) {
      this.updatedAt = source === "db"
        ? new Date(JSON.parse(this.updatedAt))
        : new Date(this.updatedAt);
    }
    if (!!this.lastParsed && !_.isDate(this.lastParsed)) {
      this.lastParsed = source === "db"
        ? new Date(JSON.parse(this.lastParsed))
        : new Date(this.lastParsed);
    }
  }

  /**
	 * setCreatedAt() sets the createdAt field
	 * to Date.now();
     * 
	 */
  setCreatedAt() {
    if (!this.createdAt) {
      this.createdAt = new Date();
    }
    return this;
  }

  /**
	 * setUpdatedAt() sets the updatedAt field to
	 * Date.now();
	 */
  setUpdatedAt() {
    this.updatedAt = new Date();
    return this;
  }

  /**
	 * setLastParsed() sets the lastParsed field to
	 * Date.now();
	 */
  setLastParsed() {
    this.lastParsed = new Date();
    return this;
  }

  /**
	 * setEtag() sets the etag of the parsed entity;
	 */
  setEtag() {
    this.etag = this._genEtag(this);
    return this;
  }

  /**
     * getEtag() returns the etag field of this entity;
     * 
     * @return {string}
     */
  getEtag() {
    if (!this.etag) {
      this.setEtag();
    }
    return this.etag;
  }

  /**
	 * isEmpty() checks wether this entity is empty
	 * @return {Boolean} 
	 */
  isEmpty() {
    return !(this.url && (this.createdAt || this.updatedAt));
  }

  /**
	 * toJSON() transforms this instance to a plain
	 * json object;
	 * @return {Object} 
	 */
  toJSON() {
    let jsoned = _.pick(this, [
      "extracted",
      "etag",
      "state",
      "createdAt",
      "updatedAt",
      "lastParsed",
      "isRelevant",
      "isNotFound",
      "isSuccessfullParse",
      "spider",
      "url"
    ]);
    return jsoned;
  }

  /**
	 * hasChanged() checks wether the given entity has changed
	 * based on the previous etag of the entity
     * 
	 * @param  {Entity}  entity
	 * @param  {string}  oldEtag  
	 * @return {boolean}          
	 */
  hasChanged(oldEtag) {
    let entityEtag = this.getEtag();
    return entityEtag !== oldEtag;
  }

  /**
	 * fromJSON() generates an Entity instance from a 
	 * json object; typically the extracted object after
	 * crawling 
     * 
	 * @param  {Object} crawledEntity 
     * @abstract
	 */
  static async fromJSON(crawledEntity) {}

  /**
	 * fromDB() generates an Entity from the database
	 * serialized object;
     * 
	 * @param  {Object} dbProperty 
     * @abstract           
	 */
  static async fromDB(dbProperty) {}

  /**
     * fromBackend instantiates the entity with the values taken
     * from the API
     * @param {Object} apiEntity
     * @abstract
     */
  static async fromBackend(apiEntity) {}
}

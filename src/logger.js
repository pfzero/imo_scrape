import winston from 'winston';

export default function init() {
	winston.level = 'debug';
	winston.remove(winston.transports.Console);
	winston.add(winston.transports.Console, { timestamp: true });
}
export default function getCategoryKey(categoriesMapping, category) {
  let transformedMapping = {};
  Object.keys(categoriesMapping).forEach(key => {
    transformedMapping[key.toLowerCase()] = categoriesMapping[key];
  });
  return transformedMapping[category.toLowerCase()];
}

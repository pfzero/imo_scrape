import moment from 'moment';

/**
 * strToDate() transforms strings like '1 week' (ago)
 * or '1 month', '2 months' to their date equivalent
 * in the past
 * @example
 * 	 // assuming current date is 15 Nov 2016
 * 	 let pastWeek = strToDate('1 week');
 * 	 console.log(pasWeek) // 8 Nov 2016
 * @param  {String} str
 * @return {Date}
 */
export default function strToDate(str) {
	let timespanVals = str.split(' ');
	return moment().subtract(timespanVals[0], timespanVals[1]).toDate();
};

import { isObject } from "lodash";

const objectString = "[object Object]";

export default function isObj(maybeObject) {
  return isObject(maybeObject) && maybeObject.toString() === objectString;
}

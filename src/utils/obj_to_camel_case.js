import _ from "lodash";
import isObj from "./is_object";

export default function objToCamelCase(obj) {
  let transformed = {};
  if (!isObj(obj)) {
    return obj;
  }
  Object.keys(obj).forEach(key => {
    let newKey = _.camelCase(key);
    if (isObj(obj[key])) {
      transformed[_.camelCase(key)] = objToCamelCase(obj[key]);
    } else if (_.isArray(obj[key])) {
      transformed[_.camelCase(key)] = obj[key].map(item => {
        if (isObj(item)) {
          return objToCamelCase(item);
        }
        return item;
      });
    } else {
      transformed[_.camelCase(key)] = obj[key];
    }
  });
  return transformed;
}

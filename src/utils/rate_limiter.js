/**
 * RateLimiter is a simple, naive rate limiter implementation;
 * It accepts any function that returns a promise;
 * @example
 * 	limiter = new RateLimiter(1000); // 1 req per second
 * 	[0..5].forEach(i => {
 * 		limiter.add(() => console.log(i));
 * 	}); // will print numbers 1-5 at interval of 1 second each;
 */
export default class RateLimiter {
	constructor(rate) {
		this.rate = rate;
		this.interval = null;
		this.requests = [];
		this.__startListening();
	}

	add(fn) {
		return new Promise((resolve, reject) => {
			this.requests.push({
				fn,
				resolve,
				reject
			});
		});
	}

	__startListening() {
		this.interval = setInterval(this.__resolveNext.bind(this), this.rate);
	}

	async __resolveNext() {
		let req = this.requests[0];
		if (!req) {
			return;
		}
		this.requests.splice(0, 1);
		try {
			let resp = await req.fn();
			req.resolve(resp);
		} catch (e) {
			req.reject(e);
		}
	}
}

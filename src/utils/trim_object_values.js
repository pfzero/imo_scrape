import {isArray} from 'lodash';
import isObj from './is_object';

function trimValue(val) {
	if (typeof val === 'string') {
		val = val.trim();
	} else if (isObj(val)) {
		val = trimObjectValues(val);
	} else if (isArray(val)) {
		val = val.map(trimValue);
	}
	return val;
}

export default function trimObjectValues(obj) {
	let trimmed = {};
	Object.keys(obj).forEach(key => {
		trimmed[key] = trimValue(obj[key]);
	});
	return trimmed;
}

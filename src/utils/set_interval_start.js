

export default function setIntervalStart(fn, duration = 0) {
    fn();
    let intervalId = setInterval(fn, duration);
    return intervalId;
}
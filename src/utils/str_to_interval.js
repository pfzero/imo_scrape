import moment from 'moment';

/**
 * strToInterval returns the number of milliseconds
 * between current date and the given string which 
 * gets evaluated in the past;
 * @example
 * 	// assuming current date is 15 Nov 2016
 * 	let interval = strToInterval('1 week');
 * 	console.log(interval) // 259200000 -> number of milliseconds
 * 						  // between current date (15 Nov) and
 * 						  // 8 Nov -- 1 week before;
 * @param  {String} str
 * @return {Number}
 */
export default function strToInterval(str) {
	let vals = str.split(' ');
	let past = moment().subtract(vals[0], vals[1]);
	let current = moment();
	return current.diff(past);
}

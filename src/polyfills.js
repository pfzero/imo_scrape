var Promise = require('bluebird');

// promise polyfill for better
// performance for promises;
global.Promise = Promise;
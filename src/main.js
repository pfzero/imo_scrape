import './register';

// general modules
import logger from 'winston';
import onDeath from 'death';
import path from 'path';
// custom modules
import Daemon from 'core/daemon';
import loggerInit from 'logger';

global.APP_PATH = path.resolve(__dirname + '/../');

// initialization fn
async function init() {
	try {
		loggerInit();
	} catch (e) {
		console.error(e && e.stack);
	}
}

// listen for OS shutdown signals and handle them
// propeprly
function listenForShutdown(daemon) {
	onDeath(async(signal, err) => {
		try {
			logger.info('stopping daemon...');
			await daemon.stop();
			logger.info('daemon stopped');
			process.exit(0);
		} catch (e) {
			logger.error('failed to stop daemon');
			logger.error(e.stack);
			process.exit(1);
		}
	});
}

async function main() {

	try {
		await init();
	} catch (e) {
		console.error('initialization failed');
		console.error('shutting down app...');
	}
	logger.info('initialization done...');
	logger.info('hell yeah! starting daemon...');

	let daemon;
	try {
		daemon = new Daemon();
		listenForShutdown(daemon);
		await daemon.start();
		logger.info('daemon up and running...');
	} catch (e) {
		logger.error('failed to start daemon...');
		logger.error(e);
		process.exit(1);
	}
}

main();

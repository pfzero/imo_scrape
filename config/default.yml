# app metadata
app:
  name: 'Imo Scraper'

# general crawler settings
# https://github.com/cgiffard/node-simplecrawler#configuration
crawler_general_settings:
  # (ms) The interval with which the crawler will spool up new requests (one per tick).
  interval: 2000
  # The maximum number of requests the crawler will run simultaneously. 
  # Defaults to 5 - the default number of http agents node will run.
  maxConcurrency: 2
  # The maximum time in milliseconds the crawler will wait for headers
  # before aborting the request.
  timeout: 8000
  # The maximum time in milliseconds the crawler will wait for async listeners.
  listenerTTL: 10000
  # The user agent the crawler will report.
  userAgent: "Mozilla/5.0 (Linux; compatible; universal/2.0)"
  # Response bodies that are compressed will be automatically decompressed 
  # before they're emitted in the fetchcomplete event. Even if this is falsy, 
  # compressed responses will be decompressed before they're passed to the 
  # discoverResources method.
  decompressResponses: true
  # Response bodies will be intelligently character converted to standard 
  # JavaScript strings using the iconv-lite module. The character encoding 
  # is interpreted from the Content-Type header firstly, and secondly from 
  # any <meta charset="xxx" /> tags.
  decodeResponses: false
  # Controls whether the crawler should respect rules in robots.txt 
  # (if such a file is present). The robots-parser module is used to do the 
  # actual parsing. This property will also make the default crawler.discoverResources 
  # method respect <meta name="robots" value="nofollow"> tags - meaning that no 
  # resources will be extracted from pages that include such a tag.
  respectRobotsTxt: true
  # If the response for the initial URL is a redirect to another domain 
  # (e.g. from github.net to github.com), update crawler.host to continue 
  # the crawling on that domain.
  allowInitialDomainChange: false
  # Specifies whether the crawler will restrict queued requests to a given domain/domains.
  filterByDomain: true
  # Enables scanning subdomains (other than www) as well as the specified domain.
  scanSubdomains: false
  # Treats the www domain the same as the originally specified domain.
  ignoreWWWDomain: true
  # Or go even further and strip WWW subdomain from requests altogether!
  stripWWWDomain: false
  # Specify to strip querystring parameters from URL's.
  stripQuerystring: false
  # Whether to scan for URL's inside HTML comments. Only applicable if the 
  # default discoverResources function is used.
  parseHTMLComments: true
  # Whether to scan for URL's inside script tags. Only applicable if 
  # the default discoverResources function is used.
  parseScriptTags: true
  # Specify a cache architecture to use when crawling. Must implement SimpleCache interface. 
  # You can save the site to disk using the built in file system cache like this:
  # crawler.cache = new Crawler.cache('pathToCacheDirectory');
  # cache: needs code-level implementation

  # The crawler should use an HTTP proxy to make its requests.
  useProxy: false
  proxyHostname: "127.0.0.1"
  proxyPort: 8123
  # The username for HTTP/Basic proxy authentication (leave unset for 
  # unauthenticated proxies.)
  proxyUser: null
  proxyPass: null

  # An array of domains the crawler is permitted to crawl from. 
  # If other settings are more permissive, they will override this setting.
  # domainWhitelist: 

  # An array of RegExp objects used to determine whether a URL protocol 
  # is supported. This is to deal with nonstandard protocol handlers 
  # that regular HTTP is sometimes given, like feed:. It does not provide 
  # support for non-http protocols (and why would it!?)
  # allowedProtocols: []

  # The maximum resource size that will be downloaded, in bytes. 
  # Defaults to 16MB.
  maxResourceSize: 16777216

  # An array of RegExp objects used to determine what MIME 
  # types simplecrawler should look for resources in. If 
  # crawler.downloadUnsupported is false, this also 
  # restricts what resources are downloaded.
  # supportedMimeTypes: []

  # simplecrawler will download files it can't parse 
  # (determined by crawler.supportedMimeTypes). 
  # Defaults to true, but if you'd rather save the 
  # RAM and GC lag, switch it off. When false, it 
  # closes sockets for unsupported resources.
  downloadUnsupported: true

  # Flag to specify if the domain you are hitting 
  # requires basic authentication.
  needsAuth: false
  authUser: ""
  authPass: ""
  
  # An object specifying a number of custom headers 
  # simplecrawler will add to every request. These 
  # override the default headers simplecrawler sets, 
  # so be careful with them. If you want to tamper 
  # with headers on a per-request basis, see the fetchqueue event.
  # customHeaders: 
    # x-crawler-info: "made with ♥"
  
  # Flag to indicate if the crawler should hold on to cookies.
  acceptCookies: true

  # Set this to iso8859 to trigger URI.js' re-encoding of 
  # iso8859 URL's to unicode.
  urlEncoding: "unicode"

  # Defines a maximum distance from the original request 
  # at which resources will be downloaded.
  # examples: 
  #   maxDepth: 1 // Only first page is fetched (with linked CSS & images)
  maxDepth: 0

  # Treat self-signed SSL certificates as valid. SSL certificates will 
  # not be validated against known CAs. Only applies to https requests. 
  # You may also have to set the environment variable NODE_TLS_REJECT_UNAUTHORIZED 
  # to '0'. For example: process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';
  ignoreInvalidSSL: false

  # flag indicating wether the crawler should
  # sync the queue with a fs file;
  useFsCache: true

  # the directory where each crawler should store
  # it's queue
  queueDir: '.data'

  # List of page conditions to determine wether the page in queue is worth fetching / downloading;
  # this can be overwritten per crawler configuration
  fetchConditions:
    allow:
      contentTypes: 
        - text/html
    forbid:
      contentTypes: 
        - application/javascript

      # e.g. \.pdf
      extensions:
        - \.pdf
        - \.jpg
        - \.jpeg
        - \.png
        - \.js
        - \.css
        - \.woff

  # relevantPages are the pages that relevant to each crawler;
  # this should be overwritten per crawler config
  # relevantPages:

# crawlers crons
crawler_schedulers:
  # timespan until a property gets recrawled
  reCrawlTimespan: 1 week

  # timespan to restart the crawlers
  restart: 3 days

  # timespan until all the properties are checked again
  # to be recrawled;
  reCrawlProperties: 1 day

  # ping each spider intervals
  pingSpider: 1 minute

# list of crawlers
crawlers:
  - 
    # host we need to start the crawling
    host: "http://sales.citr.ro/"
    # name of the crawler
    name: "CITR"
    # unique key for this crawler
    key: "citr"
    # type of the crawler - available types are: estates and estate_ads
    type: "estates"
    enabled: true
    # overwrite / enhance crawler configuration if needed
    settings:
      relevantPages:
        - /detalii-produs/.*
  - 
    host: "http://www.anuntbursa.ro"
    name: "Anunturi Bursa"
    key: "anunturi_bursa"
    type: "estate_ads"
    # debug: true
    enabled: true
    # overwrite / enhance crawler configuration if needed
    settings:
      relevantPages:
        - /Anunturi_Imobiliare/Vanzari/.*/&anunt=.*
        - /Licitatii/&anunt=.*
  - 
    host: "http://romanialibera.ro/publicitate"
    name: "Anunturi Romania Libera"
    key: "anunturi_romanialibera"
    type: "estate_ads"
    debug: false
    enabled: true
    # overwrite / enhance crawler configuration if needed
    settings:
      relevantPages:
        - /publicitate/anunt/.*

# service configs
services:
  # nominatim service configurations; - open street map service
  nominatim:
    # rate limit the requests; 1 request / 1500 ms
    rateLimit: 500
    base: http://nominatim.openstreetmap.org/
    reverseGeo: http://nominatim.openstreetmap.org/reverse?format=json&lat=${lat}&lon=${lng}&zoom=18&addressdetails=1
  # backend api configs;
  imo_api:
    # rate limit the requests; 1 request / 400ms
    rateLimit: 400
    token: "Ocr-HoS0QGiHUAnIheKgAg"
    endpoints:
      base: "http://localhost:3000/api"
      heartbeat: "http://localhost:3000/api/heartbeat"
      crawledEstate:
        base: "/crawled-estate"
        create: ""
        update: "/id/${id}"
        getByField: "/by?field=${field}&value=${value}"
        getPropertiesToRecrawl: "/to-parse?spider_name=${spiderName}"
        getCrawledProperties: "/parsed?spider_name=${spiderName}"
      crawledEstateAd:
        base: "/crawled-estate-ad"
        create: ""
        update: "/id/${id}"
        getByField: "/by?field=${field}&value=${value}"
        getEstateAdsToRecrawl: "/to-parse?spider_name=${spiderName}"
        getCrawledEstateAds: "/parsed?spider_name=${spiderName}"
      spider:
        base: "/spider"
        create: "?spider_name=${spiderName}&spider_description=${spiderDescription}&spider_type=${spiderType}&base_url=${host}"
        ping: "/ping?spider_name=${spiderName}"

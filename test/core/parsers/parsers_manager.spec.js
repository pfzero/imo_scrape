import '../../../src/register';
import test from 'tape';
import ParsersManager from '../../../src/core/parsers/parsers_manager';
import fs from 'fs';

test('parsers manager integration', async(t) => {
	const html = fs.readFileSync(__dirname + '/sample/citr.property.html');
	let mgr = new ParsersManager();
	let parsed = await mgr.parsePage({ key: 'citr' }, { url: 'some-citr-url' }, html);
	console.log(parsed);
});
